sap.ui.define([
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/core/BusyIndicator"
], function(MessageBox, MessageToast, BusyIndicator) {
	"use strict";
	
	return {
		
		console : console,
		
		_showBI: function(value){
			if(value){
				BusyIndicator.show(0);
			} else {
				BusyIndicator.hide();
			}
		},
		
		_isEmpty: function(object){
			if(object === null || object === ""){
				return true;
			} else {
				return false;	
			}
		},
		
		_showConsole: function(_message, _type) {
			try{
				if(_message !== undefined && _type !== undefined){
					if (_type === "info") {
						this.console.info(_message);
					} else if (_type === "error") {
						this.console.error(_message);
					} else if (_type === "warn") {
						this.console.warn(_message);
					} else if (_type === "done") {
						this.console.log("%c" + _message , "color: Green ");
					}
				}else{
					this.console.warn("_message or _type are undefined");
				}
			} catch(err){
				this.console.warn(err.stack);
			}
		},
		
		_onShowMessage: function(_message, _type) {
			try{
				if(_message !== undefined && _type !== undefined){
					if (_type === "info") {
						MessageBox.information(_message);
					} else if (_type === "error") {
						MessageBox.error(_message);
					} else if (_type === "warn") {
						MessageBox.warning(_message);
					} else if (_type === "toast") {
						MessageToast.show(_message);
					} else if (_type === "done") {
						MessageBox.success(_message);
					}
				}else{
					this.console.warn("_message or _type are undefined");
				}
			} catch(err){
				this.console.warn(err.stack);
			}
		},
		
		_showMessage: function(_state, _oMessage) {
			try{
				if(_state !== undefined && _oMessage !== undefined){
					var message = "\n";
					for (var i = 0; i < _oMessage.length; i++) {
						if (_oMessage[i].message === undefined){
							message = _oMessage;
						} else {
							message = message + _oMessage[i].message + "\n\n";
						}
					}
					if (_state === 1) {
						MessageBox.success(message);
					}
					if (_state === 2) {
						MessageToast.show(message);
					}
					if (_state === 3) {
						MessageBox.error(message);
					}	
				} else {
					this.console.warn("_state or _oMessage are undefined");
				}
			} catch(err){
				this.console.warn(err.stack);
			}
        }
        
	};
	
});