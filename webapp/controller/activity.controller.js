sap.ui.define([
	"sap/ui/core/ValueState",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"mqa/ui5/mq/util/Util"
], function(ValueState, Controller, JSONModel, Util) {
	"use strict";

	return Controller.extend("mqa.ui5.mq.controller.activity", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf mqa.ui5.mq.view.activity
		 */
		oDataModel: {
			hcp: new sap.ui.model.odata.ODataModel("/HANA_DB/logical/service/service.xsodata/")
				//erp: new sap.ui.model.odata.ODataModel(" /sap/opu/odata/sap/ZHOJANEGOCIO_SRV/")
		},
		onInit: function() {
			var sRootPath = jQuery.sap.getModulePath("mqa.ui5.mq");
			//var oModel = new JSONModel([sRootPath, "/model/model.json"].join("/"));
			this.oModel = new JSONModel({
				actividad: {
					ID_ACTIVIDAD: 1,
					ID_EQUIPO: "",
					//ID_PROYECTO: "02",
					TIPO_IMPUTACION: "",
					OBJETO_IMPUTACION: "",
					HORA_INICIO: null,
					HORA_FIN: null,
					HOROMETRO_INICIAL: 0,
					HOROMETRO_FINAL: 0,
					TIPO_ACTIVIDAD: "",
					ACTIVIDAD: "",
					TIPO_UBICACION: "",
					INICIO_ABSCISA: "",
					FIN_ABSCISA: "",
					VALE_INTERNO: "",
					VALE_PROVEEDOR: ""
				},
				activities: [],
				combosData: {
					materiales: [],
					almacenes: [],
					detalle_actividad: []
				},
				ID_EQUIPO:null
			});

			//oModel.setSizeLimit(99999);
			this.getView().setModel(this.oModel);
			sap.ui.getCore().setModel(this.oModel, "actividad");
			//oModel.setSizeLimit(99999);
			//this.getView().setModel(oModel);
			this.readData();

		},

		addActivity: function() {
			this._openActivity();
		},

		_openActivity: function() {
			// validate dialog lazily
			if (!this._oActivity) {
				// create dialog via fragment factory
				this._oActivity = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.activity", this);
				this._getView().addDependent(this._oActivity);
			}
			this._oActivity.open();
		},

		_closeActivity: function() {
			this._oActivity.close();
		},

		_saveActivity: function() {
			var oModel = this.getView().getModel();
			var oData = oModel.getProperty("/actividad");
			var modelo = [];
			var obj = {
				ID_ACTIVIDAD:1,
				ID_EQUIPO: oModel.getProperty("/ID_EQUIPO"),
				TIPO_IMPUTACION: oData.TIPO_IMPUTACION,
				OBJETO_IMPUTACION: oData.OBJETO_IMPUTACION,
				HORA_INICIO: oData.HORA_INICIO,
				HORA_FIN: oData.HORA_FIN,
				HOROMETRO_INICIAL: oData.HOROMETRO_INICIAL,
				HOROMETRO_FINAL: oData.HOROMETRO_FINAL,
				TIPO_ACTIVIDAD: oData.TIPO_ACTIVIDAD,
				ACTIVIDAD: oData.ACTIVIDAD,
				TIPO_UBICACION: oData.TIPO_UBICACION,
				INICIO_ABSCISA: oData.INICIO_ABSCISA,
				FIN_ABSCISA: oData.FIN_ABSCISA,
				VALE_INTERNO: oData.VALE_INTERNO,
				VALE_PROVEEDOR: oData.VALE_PROVEEDOR
			};
			modelo.push(obj);
			for (var i = 0; i < modelo.length; i++) {
				modelo[i].HOROMETRO_INICIAL = parseFloat(modelo[i].HOROMETRO_INICIAL);
				modelo[i].HOROMETRO_FINAL = parseFloat(modelo[i].HOROMETRO_FINAL);
			modelo = JSON.stringify(modelo);	
			//Guardar Actividad.
			jQuery.ajax({
				url: "/HANA_DB/logical/xs_service/sincronizar_actividad.xsjs",
				method: "POST",
				async: false,
				data:modelo,
				success: function(oResponse) {
					console.log(oResponse);
				},
				error: function(oError) {
					console.log(oError);
				}
			});
			//Convertir a Json el array.
			modelo = JSON.parse(modelo);
			}
			oModel.setProperty("/activities", modelo);
			oModel.setProperty("/actividad", []);
			this._oActivity.close();
			/*var oActivity = {
						"imputation_type": this._getCore().byId("imputation_type").getSelectedItem().getText(),
						"imputation_object": this._getCore().byId("imputation_object").getValue(),
						"start_hour": this._getCore().byId("start_hour").getValue(),
						"final_hour": this._getCore().byId("final_hour").getValue(),
						"start_horometer": this._getCore().byId("start_horometer").getValue(),
						"final_horometer": this._getCore().byId("final_horometer").getValue(),
						"activity_type": this._getCore().byId("activity_type").getSelectedItem().getText(),
						"activity": this._getCore().byId("activity").getValue(),
						"location_type": this._getCore().byId("location_type").getValue(),
						"start_abscissa": this._getCore().byId("start_abscissa").getValue(),
						"final_abscissa": this._getCore().byId("final_abscissa").getValue(),
						"in_no_vale": this._getCore().byId("in_no_vale").getValue(),
						"out_no_vale": this._getCore().byId("out_no_vale").getValue()
					};
			
					oActivities.push(oActivity);
					this._getView().getModel().updateBindings();*/
		},

		addToll: function() {
			this._openToll();
		},

		_openToll: function() {
			// validate dialog lazily
			if (!this._oToll) {
				// create dialog via fragment factory
				this._oToll = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.toll", this);
				this._getView().addDependent(this._oToll);
			}
			this._oToll.open();
		},

		_closeToll: function() {
			this._oToll.close();
		},

		_saveToll: function() {
			var oModel = this.getView().getModel();
			var data = oModel.getProperty("/activities");
			for (var i = 0; i < data.length; i++) {
				data[i].HOROMETRO_INICIAL = parseFloat(data[i].HOROMETRO_INICIAL);
				data[i].HOROMETRO_FINAL = parseFloat(data[i].HOROMETRO_FINAL);
			data = JSON.stringify(data);	
			//Guardar Actividad.
			jQuery.ajax({
				url: "/HANA_DB/logical/xs_service/sincronizar_actividad.xsjs",
				method: "POST",
				async: false,
				data:data,
				success: function(oResponse) {
					console.log(oResponse);
				},
				error: function(oError) {
					console.log(oError);
				}
			});
			//Convertir a Json el array.
			data = JSON.parse(data);
			}
			this._closeActivity();
		},
		readData: function() {
			var actividad = [];
			var oModel = this.getView().getModel();
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			var filtro = "$filter= ID_EQUIPO eq '"+ id_equipo +"'";
			var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({pattern: "KK:mm:ss a"});  
			this.oDataModel.hcp.read("T_ACTIVIDAD?" + filtro, null, null, false,
				function(oData) {
					actividad = oData.results;
					for(var i = 0; i < actividad.length; i++){
						actividad[i].HORA_INICIO = timeFormat.format(new Date(actividad[i].HORA_INICIO.ms));
						actividad[i].HORA_FIN = timeFormat.format(new Date(actividad[i].HORA_FIN.ms));
					}
				},
				function(oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/activities", actividad);
			
			var materiales = [];
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO", null, null, false,
				function(oData) {
					materiales = oData.results;
				},
				function(oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/materiales", materiales);

			var almacenes = [];
			this.oDataModel.hcp.read("T_ALMACENES", null, null, false,
				function(oData) {
					almacenes = oData.results;
				},
				function(oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/almacenes", almacenes);

			var detalle = [];
			this.oDataModel.hcp.read("T_DETALLES_ACTIVIDAD", null, null, false,
				function(oData) {
					detalle = oData.results;
				},
				function(oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/detalle_actividad", detalle);
		},

		/* _getCore
		 * @returns {sap.ui.core} the core instance
		 * Convenience method for getting the core controller of the application.
		 */
		_getCore: function() {
			return sap.ui.getCore();
		},

		/* _getModel
		 * @returns {sap.ui.model.Model} the model instance
		 * Convenience method for getting the view model by name in every controller of the application.
		 */
		_getModel: function() {
			return this.getView().getModel();
		},

		/* _getView
		 * @returns {sap.ui.model.View} the View instance
		 * Convenience method for getting the view model by name in every controller of the application.
		 */
		_getView: function() {
			return this.getView();
		}

	});
});