sap.ui.define([
	"sap/ui/core/ValueState",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"mqa/ui5/mq/util/Util"
], function (ValueState, Controller, JSONModel, Util) {
	"use strict";
	
	var toNext = true;

	return Controller.extend("mqa.ui5.mq.controller.home", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf mqa.ui5.mq.view.home
		 */
		 
		 hanadb: "/HANA_DB/logical",
		 
		oDataModel: {
			hcp: new sap.ui.model.odata.ODataModel("/HANA_DB/logical/service/service.xsodata/"),
			hcp2: new sap.ui.model.odata.ODataModel("/HANA_DB/logical/xs_service/"),
			erp: new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZMQ_SRV/"),
			erpProc: new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZPROCURA_SRV/"),
			s4h: new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/"),
		},
		onInit: function () {
			var sRootPath = jQuery.sap.getModulePath("mqa.ui5.mq");
		},
		onAfterRendering: function () {
			var oModel = new JSONModel({
				informacion_general: {
					ID_INFORMACION: 1,
					ID_EQUIPO: "",
					PROYECTO: "",
					OPERADOR: "",
					HOROMETRO_INICIAL: 0,
					HOROMETRO_FINAL: 0,
					KILOMETRO_INICIAL: 0,
					KILOMETRO_FINAL: 0,
					FECHA_MQ: new Date(),
					MQ_CONSECUTIVO: ""
				},
				lubricantes: {
					ID_LUBRICANTE: 1,
					ID_EQUIPO: "",
					LUBRICANTE: 0,
					CANTIDAD: 0,
					UNIDAD: "",
					VALOR_HOROMETRO: 0,
					VALOR_ODOMETRO: 0,
					NUMERO_VALE: 0,
					ALMACEN: null,
					MQ_CONSECUTIVO: ""
				},
				desgaste: {
					ID_DESGASTE: 1,
					ID_EQUIPO: "",
					ELEMENTO: "",
					CANTIDAD: 0,
					UNIDAD: "",
					VALOR_HOROMETRO: 0,
					VALOR_ODOMETRO: 0,
					NUMERO_VALE: 0,
					ALMACEN: null,
					MQ_CONSECUTIVO: ""
				},
				fallas: {
					ID_FALLA: 1,
					ID_EQUIPO: "",
					PARTE_OBJETO: "",
					CLASE_INCIDENTE: "",
					INCIDENTE: "",
					FECHA_INCIDENTE: new Date(),
					OBSERVACIONES: "",
					MQ_CONSECUTIVO: ""
				},
				aprobaciones: {
					ID_APROBACION: 1,
					ID_EQUIPO: "",
					TIPO_APROBACION: "",
					USUARIO: "",
					FECHA_APROBACION: new Date(),
					ESTADO_APROBACION: "",
					OBSERVACIONES: "",
					MQ_CONSECUTIVO: ""
						//ESTADISTA: ""
				},
				combustible: {
					ID_COMBUSTIBLE: 1,
					ID_EQUIPO: "",
					COMBUSTIBLE: 0,
					CANTIDAD: 0,
					VALOR_HOROMETRO: 0,
					NUMERO_VALE: 0,
					VALOR_ODOMETRO: 0,
					ALMACEN: null,
					MQ_CONSECUTIVO: ""
				},
				peaje: {
					title: "peaje",
					ID_DETALLE: 1,
					MQ_CONSECUTIVO: "",
					ID_ACTIVIDAD: 1,
					ESTACION_PEAJE: null,
					CATEGORIA: null,
					TIPO_PEAJE: "",
					HORA: "",
					MATERIAL_SAP: "",
					VALOR: 0,
					FECHA_PEAJE: new Date(),
					ID_EQUIPO: ""
				},
				actividad: {
					ID_ACTIVIDAD: 1,
					ID_EQUIPO: "",
					TIPO_IMPUTACION: "",
					PLANTA: "",
					ORDEN: "",
					PROYECTO: "",
					TRAMO: "",
					ACTIVIDAD: "",
					SUBACTIVIDAD: "",
					HORA_INICIO: null,
					HORA_FIN: null,
					HOROMETRO_INICIAL: 0,
					HOROMETRO_FINAL: 0,
					TIPO_UBICACION: "",
					INICIO_ABSCISA: "",
					FIN_ABSCISA: "",
					VALE_INTERNO: "",
					VALE_PROVEEDOR: "",
					clave_modelo: ""
				},
				validate: {
					kilometro_inicial: "None",
					kilometro_final: "None",
					horometro_inicial: "None",
					horometro_final: "None",
					estado_aprobacion: "None",
					tipo_aprobacion: "None"
				},
				activities: [],
				lubricants: [],
				wear: [],
				fault: [],
				datoserp: [],
				jerarquiapep: [],
				infoPeaje: [],
				todos_componentes: [],
				todas_subactividades: [],
				todas_actividades: [],
				todos_tramos: [],
				abscisas: [],
				combosData: {
					tipo_combustible: [],
					tipo_lubricante: [],
					unidad_lubricante: [],
					elemento_desgaste: [],
					clase_incidente: [],
					tipo_aprobacion: [],
					estado_aprobacion: [],
					tipo_imputacion: [],
					estacion_peaje: [],
					parte_objeto: [],
					maestroPeaje: [],
					incidente: [],
					proyecto: [],
					operador: [],
					tipo_peaje: [],
					almacenes: [],
					detalle_actividad: [],
					tipoFuel: [],
					plantas: [],
					proyectos: [],
					pep: [],
					_actividad: [],
					orden_planta: [],
					componente: [],
					abscisas: []
				},
				fuels: [],
				equipos: [],
				mqs: [],
				userInfo: null,
				ID_EQUIPO: null,
				MQ_CONSECUTIVO: "",
				ODOMETRO_EQUIPO: "",
				AUFNR_PLANTA: "",
				GRAFO_ACT: "",
				OPERACION_ACT: "",
				COD_ACT: "",
				CENTRO: "",
				TEXTO_CLASIFICACION: "",
				material: "",
				rol: 0,
				horas_trabajadas: 0,
				cantidad_max: 0,
				cantidad_max_lubr: 0,
				cantidad_max_desg: 0,
				fecha_mq: null,
				hora_inicio: null,
				hora_fin: null,
				prom_horometro: 0,
				prom_odometro: 0,
				id_actividad: null,
				bNeedLoadWBS: false // flag para determinar si deben buscarse los elementos PEP en la carga de registros de MQs
			});
			oModel.setSizeLimit(99999);
			this.getView().setModel(oModel);
			this.loadUserInfo();
			this.cargarRoles();
			this.cargarCombos();
			this.consultarCds();
			this.obtenerPlantas();
			console.log("carga");
		},
		//Función para limpiar todo el modelo del MQ visualmente.
		setModelEmpty: function () {
			var oModel = this.getView().getModel();
			var informacion_general = {
				ID_INFORMACION: 1,
				ID_EQUIPO: "",
				PROYECTO: "",
				OPERADOR: "",
				HOROMETRO_INICIAL: 0,
				HOROMETRO_FINAL: 0,
				KILOMETRO_INICIAL: 0,
				KILOMETRO_FINAL: 0,
				FECHA_MQ: new Date(),
				MQ_CONSECUTIVO: ""
			};
			oModel.setProperty("/informacion_general", informacion_general);
			var aprobaciones = {
				ID_APROBACION: 1,
				ID_EQUIPO: "",
				TIPO_APROBACION: "",
				USUARIO: "",
				FECHA_APROBACION: new Date(),
				ESTADO_APROBACION: "",
				OBSERVACIONES: "",
				MQ_CONSECUTIVO: ""
			};
			oModel.setProperty("/aprobaciones", aprobaciones);
			var activities = [];
			oModel.setProperty("/activities", activities);
			var lubricants = [];
			oModel.setProperty("/lubricants", lubricants);
			var wear = [];
			oModel.setProperty("/wear", wear);
			var fuels = [];
			oModel.setProperty("/fuels", fuels);
			var fault = [];
			oModel.setProperty("/fault", fault);
		},
		//Funciones para cargar información.
		cargarEquipos: function () {
			var oModel = this.getView().getModel();
			var equipos = [];
			var cabecera = [];
			var persona;
			var puser = oModel.getProperty("/userInfo");
			var filtro = "$filter= P_USER eq '" + puser.name + "'";
			this.oDataModel.hcp.read("T_PERSONA?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						persona = oData.results[0].ID_PERSONA;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			var filtro = "$filter= ID_PERSONA eq " + persona + "";
			this.oDataModel.hcp.read("/T_ASIGNACION_EQUIPO?" + filtro, null, null, false,
				function (oData) {
					cabecera = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			for (var i = 0; i < cabecera.length; i++) {
				var filtro1 = "$filter= ID_EQUIPO eq '" + cabecera[i].ID_EQUIPO + "' &$orderby=ID_EQUIPO asc";
				this.oDataModel.hcp.read("/T_EQUIPO?" + filtro1, null, null, false,
					function (oData) {
						equipos.push(oData.results[0]);
					},
					function (oResponse) {
						console.log(oResponse);
					});
			}
			oModel.setProperty("/equipos", equipos);
		},
		//Función para realizar filtro en el listado de Equipos.
		filtrarEquipos: function (oEvent) {
			var tblMaster = this.getView().byId("ltPartners").getBinding("items");
			var sValue = oEvent.getSource().getValue();
			var oFilter2 = new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({
						path: 'ID_EQUIPO',
						operator: sap.ui.model.FilterOperator.Contains,
						value1: sValue
					})
				],
				or: true
			});
			tblMaster.filter([oFilter2]);
		},
		//Filtrar roles(no se esta utilizando).
		cargarRoles: function () {
			var text = [];
			var filtro = "$filter= ROL eq '1'";
			this.oDataModel.hcp.read("/ROL?" + filtro, null, null, false,
				function (oData) {
					text = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			for (var i = 0; i < text.length; i++) {
				var modelo = text[i].MODULO;
				var item = text[i].ITEMS;
				//this.getView().byId(item).setEnabled(false);
			}
		},
		//Leer información de usuario logueado.
		loadUserInfo: function () {
			var that = this;
			var userModel = new sap.ui.model.json.JSONModel();
			userModel.loadData("/services/userapi/attributes");
			userModel.attachRequestCompleted(function onCompleted(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.getCore().setModel(userModel, "userapi");
					var piuser = this.getData();
					window.userInfo = piuser;
					that.getView().getModel().setProperty("/userInfo", piuser);
					console.log("datos de usuario cargados con éxito");
					that.cargarEquipos();
				}
			});
		},
		//Consultar información de MQ que ya estan creados. 
		loadData: function (id_equipo, mq_consecutivo) {
			var oModel = this.getView().getModel();
			var that = this;
			var horometro;
			var odometro;
			var filtro = "$filter= ID_EQUIPO eq '" + oModel.getProperty("/ID_EQUIPO") + "'";
			this.oDataModel.hcp.read("/T_EQUIPO?" + filtro, null, null, false,
				function (oData) {
					horometro = oData.results[0].HOROMETRO;
					odometro = oData.results[0].ODOMETRO;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			if (!horometro) {
				this.getView().byId("HOROMETRO_INICIAL").setEnabled(false);
				this.getView().byId("HOROMETRO_FINAL").setEnabled(false);
			}
			if (!odometro) {
				this.getView().byId("KILOMETRO_INICIAL").setEnabled(false);
				this.getView().byId("KILOMETRO_FINAL").setEnabled(false);
			}
			var filtro = "$filter= ID_EQUIPO eq '" + id_equipo + "' and MQ_CONSECUTIVO eq '" + mq_consecutivo + "'";
			var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
				pattern: "HH:mm:ss"
			});
			var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
			var actividad = [];
			var unicosDatos = [];
			this.oDataModel.hcp.read("T_MQ_ACTIVIDAD?" + filtro, {
				async: false,
				success: function (oData) {
					//actividad = oData.results;
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							oData.results[i].HORA_INICIO = timeFormat.format(new Date(oData.results[i].HORA_INICIO.ms + TZOffsetMs));
							oData.results[i].HORA_FIN = timeFormat.format(new Date(oData.results[i].HORA_FIN.ms + TZOffsetMs));
							if (oData.results[i].TIPO_IMPUTACION === "M") {
								that.getView().byId("plantas").setVisible(true);
								that.getView().byId("orden").setVisible(true);
							} else if (oData.results[i].TIPO_IMPUTACION === "A") {
								that.getView().byId("proyect").setVisible(true);
								that.getView().byId("tramo").setVisible(true);
								that.getView().byId("actividad").setVisible(true);
							}
							if (oData.results[i].ELIMINAR !== "X") {
								actividad.push(oData.results[i]);
							}
							var dato = unicosDatos.filter(function (Txt) {
								return Txt.TRAMO === oData.results[i].TRAMO;
							});
							if (dato.length === 0) {
								unicosDatos.push(oData.results[i]);
							}
						}
					} else {}
				},
				error: function (oResponse) {
					console.log(oResponse);
				}
			});
			oModel.setProperty("/activities", actividad);
			this.useReadOmodel("T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS", null, "/todas_subactividades", "hcp");
			oModel.setProperty("/combos");

			//** INICIO sección para cargar 1 vez elementos PEP
			if (unicosDatos.length > 0) {
				var sField = "&$select=WBSDescription,WBSElement"
				var sFilter = "Version eq '' and Project eq '" + unicosDatos[0].PROYECTO + "'";
				sFilter += sField;
				var aPaths = ["/combosData/pep", "/todos_tramos"];
				this.useReadOmodel("ZXC_WBSELEMENTWITHVERSION_CDS/ZXC_WBSELEMENTWITHVERSION", sFilter, aPaths, "s4h");
				oModel.setProperty("/bNeedLoadWBS", false)
			} else {
				oModel.setProperty("/bNeedLoadWBS", true)
			}
			//** FIN sección para cargar 1 vez elementos PEP
			for (var i = 0; i < unicosDatos.length; i++) {
				this.consultarPep1(unicosDatos[i]);
				this.consultaractividad_(unicosDatos[i]);
			}
			for (var i = 0; i < actividad.length; i++) {
				if (actividad[i].COMPONENTE) {
					this.setearActividad1(actividad[i].ACTIVIDAD, actividad[i].PROYECTO);
					this.consultaSubactividades(actividad[i].PROYECTO, actividad[i].TRAMO, actividad[i].ACTIVIDAD);
				}
			}
			//Cargar Datos de Información General.
			var info = {
				ID_INFORMACION: 1,
				ID_EQUIPO: "",
				PROYECTO: "",
				OPERADOR: "",
				HOROMETRO_INICIAL: 0,
				HOROMETRO_FINAL: 0,
				KILOMETRO_INICIAL: 0,
				KILOMETRO_FINAL: 0,
				FECHA_MQ: null,
				MQ_CONSECUTIVO: ""
			};
			var proyecto;
			this.oDataModel.hcp.read("/T_MQ_INFORMACION?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						info = oData.results[0];
						proyecto = oData.results[0].PROYECTO;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/informacion_general", info);
			//Cargar Datos de Combustible.
			var combustible = [];
			this.oDataModel.hcp.read("/T_MQ_COMBUSTIBLE?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].ELIMINAR !== "X") {
								combustible.push(oData.results[i]);
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/fuels", combustible);

			//Cargar Datos de Lubricantes.
			var lubricante = [];
			this.oDataModel.hcp.read("/T_MQ_LUBRICANTES?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].ELIMINAR !== "X") {
								lubricante.push(oData.results[i]);
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/lubricants", lubricante);

			//Cargar Datos de Desgaste.
			var desgaste = [];
			this.oDataModel.hcp.read("/T_MQ_DESGASTE?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].ELIMINAR !== "X") {
								desgaste.push(oData.results[i]);
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/wear", desgaste);

			//Cargar Datos de Fallas.
			var fallas = [];
			this.oDataModel.hcp.read("/T_MQ_FALLAS?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].ELIMINAR !== "X") {
								fallas.push(oData.results[i]);
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/fault", fallas);
			//Cargar Datos de Aprobaciones.
			var aprobacion = {
				ID_APROBACION: 1,
				ID_EQUIPO: "",
				TIPO_APROBACION: "",
				USUARIO: "",
				FECHA_APROBACION: null,
				ESTADO_APROBACION: "3",
				OBSERVACIONES: "",
				MQ_CONSECUTIVO: ""
			};
			this.oDataModel.hcp.read("/T_MQ_APROBACION?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						aprobacion = oData.results[0];
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/aprobaciones", aprobacion);
			var asignacion_equipo = [];
			var filtro = "$filter= ID_EQUIPO eq '" + id_equipo + "'";
			this.oDataModel.hcp.read("T_ASIGNACION_EQUIPO?" + filtro, null, null,
				false,
				function (oData) {
					asignacion_equipo = oData.results[0];
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/CENTRO", asignacion_equipo.PROYECTO);
			//oModel.setProperty("/informacion_general/OPERADOR", asignacion_equipo.ID_PERSONA);
			oModel.setProperty("/aprobaciones/USUARIO", asignacion_equipo.ID_PERSONA);
			var centro = oModel.getProperty("/CENTRO");
			var proyecto;
			/*	var filtro1 = "$filter= CENTRO eq '" + centro + "'";
				this.oDataModel.hcp.read("T_PROYECTO?" + filtro1, null, null, false,
					function (oData) {
						if (oData.results.length > 0) {
							proyecto = oData.results[0].CODIGO_PROYECTO;
						}
					},
					function (oResponse) {
						console.log(oResponse);
					});
				oModel.setProperty("/informacion_general/PROYECTO", proyecto);*/
			var centro = oModel.getProperty("/CENTRO");
			var peajes = [];
			var filtro1 = "$filter= ID_PROYECTO eq '" + centro + "'";
			this.oDataModel.hcp.read("T_PERFIL_PEAJE?" + filtro1, null, null, false,
				function (oData) {
					peajes = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/estacion_peaje", peajes);
			var proyecto = [];
			var filter = "$filter= PROYECTO eq '" + centro + "'";
			this.oDataModel.hcp.read("T_UBICACIONES", null, null, false,
				function (oData) {
					proyecto = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/abscisas", proyecto);
		},
		//Consultar información de Materiales de Consumo (Combustible, Lubricante y Desgaste).
		cargarMateriales: function (material) {
			var oModel = this.getView().getModel();
			var combustible = [];
			var that = this;
			var filtro = "$filter= ID_MATERIALES eq '" + material + "' and TIPO_CONSUMO eq 'COMBUSTIBLE'";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						combustible = oData.results;
						oModel.setProperty("/cantidad_max", oData.results[0].CANTIDAD_MAXIMA);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/tipo_combustible", combustible);
			var lubricante = [];
			var filtro1 = "$filter= ID_MATERIALES eq '" + material + "' and TIPO_CONSUMO eq 'LUBRICANTES'";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						lubricante = oData.results;
						oModel.setProperty("/cantidad_max_lubr", oData.results[0].CANTIDAD_MAXIMA);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/tipo_lubricante", lubricante);
			var ElementoD = [];
			var filtro2 = "$filter= ID_MATERIALES eq '" + material + "' and TIPO_CONSUMO eq 'DESGASTE'";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro2, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						ElementoD = oData.results;
						oModel.setProperty("/cantidad_max_desg", oData.results[0].CANTIDAD_MAXIMA);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/elemento_desgaste", ElementoD);

		},
		//Leer información de las tablas de SCP para cargar combobox.
		cargarCombos: function () {
			var oModel = this.getView().getModel();
			var proyecto = [];
			this.oDataModel.hcp.read("T_PROYECTO", null, null, false,
				function (oData) {
					proyecto = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/proyecto", proyecto);
			var persona = [];
			this.oDataModel.hcp.read("T_PERSONA", null, null, false,
				function (oData) {
					persona = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/operador", persona);
			var tipoImputacion = [];
			this.oDataModel.hcp.read("T_TIPO_IMPUTACION", null, null, false,
				function (oData) {
					for (var i = 0; i < oData.results.length; i++) {
						if (oData.results[i].ID_IMPUTACION !== "P") {
							tipoImputacion.push(oData.results[i]);
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/tipo_imputacion", tipoImputacion);
			var tipoPeaje = [];
			this.oDataModel.hcp.read("T_TIPO_PEAJE", null, null, false,
				function (oData) {
					tipoPeaje = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/tipo_peaje", tipoPeaje);
			//Clase Incidente.
			var claseIncidente = [];
			var filtro1 = "$filter= CATALOGO eq 'D'";
			this.oDataModel.hcp.read("T_CATALOGOS?" + filtro1, null, null, false,
				function (oData) {
					claseIncidente = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/clase_incidente", claseIncidente);
			//Parte Objeto
			var parteObjeto = [];
			var filtro1 = "$filter= CATALOGO eq 'B'";
			this.oDataModel.hcp.read("T_CATALOGOS?" + filtro1, null, null, false,
				function (oData) {
					parteObjeto = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/parte_objeto", parteObjeto);
			//Incidente
			var incidente = [];
			var filtro1 = "$filter= CATALOGO eq 'C'";
			this.oDataModel.hcp.read("T_CATALOGOS?" + filtro1, null, null, false,
				function (oData) {
					incidente = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/incidente", incidente);
			var almacenes = [];
			this.oDataModel.hcp.read("T_ALMACENES", null, null, false,
				function (oData) {
					almacenes = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/almacenes", almacenes);
			var tipoCombustible = [];
			this.oDataModel.hcp.read("T_TIPO_COMBUSTIBLE", null, null, false,
				function (oData) {
					tipoCombustible = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/tipoFuel", tipoCombustible);
			var detalle = [];
			this.oDataModel.hcp.read("T_DETALLES_ACTIVIDAD", null, null, false,
				function (oData) {
					detalle = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/detalle_actividad", detalle);
		},
		//Información mostrada en Ventana Emergente luego de seleccionar un equipo
		asignData: function (oEvent) {
			var oModel = this.getView().getModel();
			// apagar flag de consulta de PEPS, por si se realiza la consulta a un nuevo equipo:
			oModel.setProperty("/bNeedLoadWBS", false);
			var oItem = oEvent.getSource(),
				id_equipo = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).ID_EQUIPO,
				material = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).MATERIAL_EQUIPO,
				odometro_equipo = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).ODOMETRO,
				texto_clasificacion = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).TEXTO_CLASIFICACION;
			this.cargarMateriales(material);
			this.ID_EQUIPO = id_equipo;
			this.getView().getModel().setProperty("/ID_EQUIPO", id_equipo);
			this.getView().getModel().setProperty("/material", material);
			this.getView().getModel().setProperty("/ODOMETRO_EQUIPO", odometro_equipo);
			this.getView().getModel().setProperty("/TEXTO_CLASIFICACION", texto_clasificacion);
			this.validarMQ(id_equipo);
			this.getView().getModel().setProperty("/actividad", []);
		},
		//Consulta información para mostrar en ventana emergente los MQ's guardados.
		validarMQ: function (id_equipo) {
			var that = this;
			var filtro1 = "$filter= ID_EQUIPO eq '" + id_equipo + "' &$orderby=ID_APROBACION desc";
			this.oDataModel.hcp.read("T_MQ_APROBACION?" + filtro1, null, null, false,
				function (oData) {
					that._openFuel();
					if (oData.results.length > 0) {
						that.getView().getModel().setProperty("/mqs", oData.results);
					} else {
						that.getView().getModel().setProperty("/mqs", []);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
		},
		//Desde ventana emergente seleccionar MQ ya creado(Consulta de Datos).
		leerMQ: function (oEvent) {
			var oItem = oEvent.getSource(),
				id_equipo = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).ID_EQUIPO,
				mq_consecutivo = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).MQ_CONSECUTIVO,
				estado_aprobacion = this.getView().getModel().getProperty(oItem._aSelectedPaths[0]).ESTADO_APROBACION;
			this.loadData(id_equipo, mq_consecutivo);
			this.getView().getModel().setProperty("/MQ_CONSECUTIVO", mq_consecutivo);
			if (estado_aprobacion === "SIN LIBERAR" || estado_aprobacion === "RECHAZADO") {
				this.deshabilitarSecciones(true);
			} else {
				this.deshabilitarSecciones(false);
				sap.m.MessageBox.success("El MQ seleccionado solo permite visualizarse.");
			}
			this._closeFuel();
		},
		//Consulta de Ubicación inicial para la pestaña de Actividades.
		setearAbscisas: function () {
			var oModel = this.getView().getModel();
			if (this._getCore().byId("proyect")) {
				var project = this._getCore().byId("proyect").getSelectedKey();
				var proyecto = [];
				var filtro1 = "$filter= PROYECTO eq '" + project + "'";
				this.oDataModel.hcp.read("T_UBICACIONES?" + filtro1, null, null, false,
					function (oData) {
						proyecto = oData.results;
					},
					function (oResponse) {
						console.log(oResponse);
					});
				oModel.setProperty("/combosData/abscisas", proyecto);
			}
		},
		//Obtener el id de la actividad 
		obtenerIdActividad: function (oEvent) {
			var id = parseInt(oEvent.getSource().getBindingContext().getPath().substr(oEvent.getSource().getBindingContext().getPath().length -
				1, 1));
			console.log(id);
			var id_actividad = this.getView().getModel().getProperty("/activities/" + id + "/ID_ACTIVITY");
			this.getView().getModel().setProperty("/id_actividad", id_actividad);
		},
		
		//Creación de un Nuevo MQ desde la pestaña de selección de fecha.
		crearMQ: function () {
			var oModel = this.getView().getModel();
			
			this.deshabilitarSecciones(true);
			
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			
			/*
			var mqs = oModel.getProperty("/mqs");
			
			//Valida MQ's sin Aprobar
			if (mqs.length > 0) {
				if (mqs[0].ESTADO_APROBACION === "SIN LIBERAR") {
					sap.m.MessageBox.error("Imposible crear, se encuentran MQ's sin liberar");
					return;
				}
			}
			
			var diasMax;
			var filtro = "$filter= DESCRIPCION_PARAMETRO eq 'DIAS_MAX_SIN_APROBAR'";
			
			this.oDataModel.hcp.read("T_PARAMETROS_APP?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						diasMax = parseFloat(oData.results[0].VALOR);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
				
			var contador = 0;
			
			for (var i = 0; i < mqs.length; i++) {
				if (mqs[i].ESTADO_APROBACION === "LIBERADO") {
					contador++;
				}
			}
			
			if (contador > diasMax) {
				sap.m.MessageBox.error("Imposible crear, se encuentran mas de " + diasMax + " MQ's sin Aprobar");
				return;
			}
			
			this.setModelEmpty();
			
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			
			this.getView().getModel().setProperty("/ID_EQUIPO", id_equipo);
			
			var asignacion_equipo = [];
			var filtro = "$filter= ID_EQUIPO eq '" + id_equipo + "'";
			
			this.oDataModel.hcp.read("T_ASIGNACION_EQUIPO?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						asignacion_equipo = oData.results[0];
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			this._closeFuel();
			
			oModel.setProperty("/CENTRO", asignacion_equipo.PROYECTO);
			oModel.setProperty("/informacion_general/PROYECTO", asignacion_equipo.PROYECTO);
			
			var centro = oModel.getProperty("/CENTRO");
			var ubicaciones = [];
			*/
			
			/* No Habilitar ===== var filtro1 = "$filter= CODIGO_PROYECTO eq '" + centro + "'";
			this.oDataModel.hcp.read("T_PROYECTO?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						proyecto = oData.results[0].CODIGO_PROYECTO;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/informacion_general/PROYECTO", proyecto); No Habilitar ===== */
			
			/*
			var peajes = [];
			var filtro = "$filter= ID_PROYECTO eq '" + centro + "'";
			
			this.oDataModel.hcp.read("T_PERFIL_PEAJE?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						peajes = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			oModel.setProperty("/combosData/estacion_peaje", peajes);
			
			var filter = "$filter= PROYECTO eq '" + centro + "'";
			
			this.oDataModel.hcp.read("T_UBICACIONES", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						ubicaciones = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			var subactividades = oModel.getProperty("/todas_subactividades");
			
			this.oDataModel.hcp.read("T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						subactividades = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			this.oDataModel.hcp.read("T_MQ_SUBACTIVIDADES_HORAS_APAGADO", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							subactividades = subactividades.concat(oData.results[i]);
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			*/
			
			var puser = oModel.getProperty("/userInfo");
			var filtro = "$filter= P_USER eq '" + puser.name + "'";
			var rol;
			var persona;
			
			this.oDataModel.hcp.read("/T_PERSONA?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						rol = oData.results[0].ID_ROL;
						persona = oData.results[0].ID_PERSONA;
					}

				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			/*
			oModel.setProperty("/rol", rol);
			oModel.setProperty("/abscisas", ubicaciones);
			oModel.setProperty("/todas_subactividades", subactividades);
			*/
			
			oModel.setProperty("/informacion_general/OPERADOR", persona);
			oModel.setProperty("/aprobaciones/USUARIO", persona);
			
			/*
			this.setearHorometro();
			*/
			
			//Open Fecha MQ
			this._openDate(id_equipo, oModel);
		},
		
		//Verificación para que no hayan MQ's iguales o la fecha supere el dia actual. 
		setearFecha: function () {
		    
			var oModel = this.getView().getModel();
			var fechamq = oModel.getProperty("/fecha_mq");
			var confirmWork;
			
			//Validaciones
			this.validations(fechamq, confirmWork);
			
			//var estadista = this._getCore().byId("estadista").getSelectedKey();
			if (fechamq <= new Date()) {
				oModel.setProperty("/informacion_general/FECHA_MQ", fechamq);
				//oModel.setProperty("/aprobaciones/ESTADISTA", estadista);
				var fecha = this.formateDate(fechamq);
				var flag = false;
				var id_equipo = oModel.getProperty("/ID_EQUIPO");
				var id_persona = oModel.getProperty("/informacion_general/OPERADOR");
				var mq_consecutivo = id_equipo + "-" + fecha + "-" + "" + id_persona + "";
				
				oModel.setProperty("/MQ_CONSECUTIVO", mq_consecutivo);
				
				var filtro = "$filter= ID_EQUIPO eq '" + id_equipo + "' and MQ_CONSECUTIVO eq '" + mq_consecutivo + "'";
				
				this.oDataModel.hcp.read("T_MQ_INFORMACION?" + filtro, null, null, false,
					function (oData) {
						if (oData.results.length > 0) {
							flag = true;
						}
					},
					function (oResponse) {
						console.log(oResponse);
					});
					
				if (flag) {
					sap.m.MessageBox.success("Ya se encuentra un MQ creado con el consecutivo '" + mq_consecutivo + "'");
					return;
				}
				
				if (toNext === true){
				    sap.m.MessageBox.success("Ya puede diligenciar el nuevo MQ.");
				}
				
				this._closeDate();
				
				if (confirmWork === false){
				    var mDataFest = oModel.getproperty("/mDataFest");
				    
				    oModel.setProperty("/informacion_general/HOROMETRO_FINAL", mDataFest[0].HOROMETRO_FINAL);
				    oModel.setProperty("/informacion_general/KILOMETRO_FINAL", mDataFest[0].KILOMETRO_FINAL);
				    
				    this.guardar();
				}
				
			} else {
				sap.m.MessageBox.success("La fecha no puede superar el dia de hoy.");
				return;
			}
		},
		
		validations: function (fechamq, confirmWork){
		    var oModel = this.getView().getModel();
			
			this.deshabilitarSecciones(true);
			
			var mqs = oModel.getProperty("/mqs");
			
			//Valida MQ's sin Aprobar
			if (mqs.length > 0) {
				if (mqs[0].ESTADO_APROBACION === "SIN LIBERAR") {
					sap.m.MessageBox.error("Imposible crear, se encuentran MQ's sin liberar");
					return;
				}
			}
			
			//Validar Festivo
			var year = fechamq.getFullYear();
			var calendar = [];
			var fecha = fechamq;
			var isFestivo = "";
		    var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern: "YYYY-MM-dd"});
			
			var filter = "?$filter=Startyear eq " + "'" + year + "'" + " and Endyear eq "
			            + "'" + year + "'" + " and Calendar eq 'CO'";
			
			//Leer Calendario ERP Con Festivos            
			this.oDataModel.erpProc.read("/CalendarioFestivosSet" + filter, null, null, false,
				function (oData) {
					oData.results.sort();
					console.log(oData.results);
					
					if (oData.results.length){
					    calendar = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			
			fecha = dateFormat.format(fecha);
			
			for (var k = 0; k < calendar.length; k++){
			    var fechaCal = calendar[k].Fecha.split("T");
			    calendar[k].Fecha = fechaCal[0];
			    
			    if (calendar[k].Fecha === fecha){
			        if (calendar[k].Festive === 'X' || calendar[k].Festivo_d === 'X'){
			            isFestivo = 'X';
			        }
			    }
			}
			
			//Activar Form 
			var formFest = sap.ui.core.Fragment.byId("idFecha", "idFestivo");
			var formActive = formFest.getVisible();
			confirmWork = null;
			
			if (isFestivo === 'X' && formActive === false){
			    formFest.setVisible(true);
			    return;
			}
			
			//Validar SI/NO Trabajo
			if (isFestivo === 'X' && formActive === true){
    			var confirmSi = sap.ui.core.Fragment.byId("idFecha", "idSi");
    			var selectSi = confirmSi.getSelected();
    			
    			if (selectSi === true){
    			    confirmWork = true;
    			}else{
    			    confirmWork = false;
    			}
			}
			
			//Días Maximos sin Registrar MQ
			var diasMax;
			var filtro = "$filter= DESCRIPCION_PARAMETRO eq 'DIAS_MAX_SIN_APROBAR'";
			
			this.oDataModel.hcp.read("T_PARAMETROS_APP?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						diasMax = parseFloat(oData.results[0].VALOR);
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			var contador = 0;
			
			fecha = new Date(fecha);
			var fechaActual = new Date();
			var diff = Math.abs(fechaActual.getTime() - fecha.getTime());
            contador = Math.ceil(diff / (1000 * 60 * 60 * 24));
			
			/*for (var i = 0; i < mqs.length; i++) {
				if (mqs[i].ESTADO_APROBACION === "LIBERADO") {
					contador++;
				}
			}*/
			
			if (contador > diasMax && !isFestivo) {
			    toNext = false;
			    
				sap.m.MessageBox.error("Imposible crear el MQ, tiene " + diasMax + " días de atraso."
				                    + " Solicite autorización para continuar con el registro de MQ's");
				return;
			}
			
			this.setModelEmpty();
			
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			
			this.getView().getModel().setProperty("/ID_EQUIPO", id_equipo);
			
			var asignacion_equipo = [];
			var filtro = "$filter= ID_EQUIPO eq '" + id_equipo + "'";
			
			this.oDataModel.hcp.read("T_ASIGNACION_EQUIPO?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						asignacion_equipo = oData.results[0];
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			this._closeFuel();
			
			oModel.setProperty("/CENTRO", asignacion_equipo.PROYECTO);
			oModel.setProperty("/informacion_general/PROYECTO", asignacion_equipo.PROYECTO);
			
			var centro = oModel.getProperty("/CENTRO");
			var ubicaciones = [];
			var peajes = [];
			var filtro = "$filter= ID_PROYECTO eq '" + centro + "'";
			
			this.oDataModel.hcp.read("T_PERFIL_PEAJE?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						peajes = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			oModel.setProperty("/combosData/estacion_peaje", peajes);
			
			var filter = "$filter= PROYECTO eq '" + centro + "'";
			
			this.oDataModel.hcp.read("T_UBICACIONES", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						ubicaciones = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			var subactividades = oModel.getProperty("/todas_subactividades");
			
			this.oDataModel.hcp.read("T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						subactividades = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			this.oDataModel.hcp.read("T_MQ_SUBACTIVIDADES_HORAS_APAGADO", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							subactividades = subactividades.concat(oData.results[i]);
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			var puser = oModel.getProperty("/userInfo");
			var filtro = "$filter= P_USER eq '" + puser.name + "'";
			var rol;
			var persona;
			
			this.oDataModel.hcp.read("/T_PERSONA?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						rol = oData.results[0].ID_ROL;
						persona = oData.results[0].ID_PERSONA;
					}

				},
				function (oResponse) {
					console.log(oResponse);
			});
			
			oModel.setProperty("/rol", rol);
			oModel.setProperty("/abscisas", ubicaciones);
			oModel.setProperty("/todas_subactividades", subactividades);
			oModel.setProperty("/informacion_general/OPERADOR", persona);
			oModel.setProperty("/aprobaciones/USUARIO", persona);
			
			this.setearHorometro();
		},
		
		//Asignar Datos de MQ consecutivo y fecha para el nuevo MQ.
		setmq: function () {
			var oModel = this.getView().getModel();
			var id_persona = oModel.getProperty("/informacion_general/OPERADOR");
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			var fechamq = oModel.getProperty("/fecha_mq");
			var fecha = this.formateDate(fechamq);
			var mq_consecutivo = id_equipo + "-" + fecha + "-" + "" + id_persona + "";
			oModel.setProperty("/MQ_CONSECUTIVO", mq_consecutivo);
			oModel.setProperty("/aprobaciones/USUARIO", id_persona);
		},
		//De acuerdo a roles habilitar o deshabilitar las secciones del aplciativo.
		deshabilitarSecciones: function (bFlag) {
			var oModel = this.getView().getModel();
			var modelo = this.getView().byId("ObjectPageLayout");
			var totalmodel = modelo.getSections();
			//Información General.
			this.getView().byId("proyecto").setEnabled(bFlag);
			this.getView().byId("operador").setEnabled(bFlag);
			var usuario = this.getView().getModel().getProperty("/informacion_general/OPERADOR");
			var puser = oModel.getProperty("/userInfo");
			var filtro = "$filter= P_USER eq '" + puser.name + "'";
			var rol;
			this.oDataModel.hcp.read("/T_PERSONA?" + filtro, null, null, false,
				function (oData) {
					rol = oData.results[0].ID_ROL;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/rol", rol);
			if (rol === 3) {
				this.getView().byId("HOROMETRO_INICIAL").setEnabled(false);
				this.getView().byId("HOROMETRO_FINAL").setEnabled(true);
				this.getView().byId("KILOMETRO_INICIAL").setEnabled(false);
				this.getView().byId("KILOMETRO_FINAL").setEnabled(true);
			} else {
				this.getView().byId("HOROMETRO_INICIAL").setEnabled(false);
				this.getView().byId("HOROMETRO_FINAL").setEnabled(bFlag);
				this.getView().byId("KILOMETRO_INICIAL").setEnabled(false);
				this.getView().byId("KILOMETRO_FINAL").setEnabled(bFlag);
			}
			var estadoAprobacion = [];
			var filtro = "$filter= ID_ROL eq " + rol + "";
			this.oDataModel.hcp.read("T_ESTADO_APROBACION?" + filtro, null, null, false,
				function (oData) {
					estadoAprobacion = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/estado_aprobacion", estadoAprobacion);
			var tipoAprobacion = [];
			this.oDataModel.hcp.read("T_TIPO_APROBACION?" + filtro, null, null, false,
				function (oData) {
					tipoAprobacion = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/tipo_aprobacion", tipoAprobacion);
			if (rol === 1) {
				this.getView().byId("tipoAprobacion").setEnabled(bFlag);
				this.getView().byId("estadoAprobacion").setEnabled(bFlag);
				this.getView().byId("observaciones").setEnabled(bFlag);
			}
			if (rol === 3) {
				bFlag = true;
			}
			this.getView().byId("fallas").getItems()
				.forEach(function (x) {
					x.getCells()
						.forEach(function (y) {
							if (y.getMetadata()._sClassName === "sap.m.ComboBox" || y.getMetadata()._sClassName === "sap.m.Input" || y.getMetadata()._sClassName ===
								"sap.m.CheckBox" || y.getMetadata()._sClassName === "sap.m.DatePicker" || y.getMetadata()._sClassName === "sap.m.TextArea"
							) {
								y.setEnabled(bFlag);
							}
						});
				});
			this.getView().byId("actividades").getRows()
				.forEach(function (x) {
					x.getCells()
						.forEach(function (y) {
							if (y.getMetadata()._sClassName === "sap.m.ComboBox" || y.getMetadata()._sClassName === "sap.m.Input" || y.getMetadata()._sClassName ===
								"sap.m.CheckBox" || y.getMetadata()._sClassName === "sap.m.TimePicker") {
								y.setEnabled(bFlag);
							}
						});
				});
			this.getView().byId("desgaste").getItems()
				.forEach(function (x) {
					x.getCells()
						.forEach(function (y) {
							if (y.getMetadata()._sClassName === "sap.m.ComboBox" || y.getMetadata()._sClassName === "sap.m.Input" || y.getMetadata()._sClassName ===
								"sap.m.CheckBox" || y.getMetadata()._sClassName === "sap.m.Select") {
								y.setEnabled(bFlag);
							}
						});
				});
			this.getView().byId("lubricantes").getItems()
				.forEach(function (x) {
					x.getCells()
						.forEach(function (y) {
							if (y.getMetadata()._sClassName === "sap.m.ComboBox" || y.getMetadata()._sClassName === "sap.m.Input" || y.getMetadata()._sClassName ===
								"sap.m.CheckBox" || y.getMetadata()._sClassName === "sap.m.DatePicker") {
								y.setEnabled(bFlag);
							}
						});
				});
			this.getView().byId("combustible").getItems()
				.forEach(function (x) {
					x.getCells()
						.forEach(function (y) {
							if (y.getMetadata()._sClassName === "sap.m.ComboBox" || y.getMetadata()._sClassName === "sap.m.Input" || y.getMetadata()._sClassName ===
								"sap.m.CheckBox" || y.getMetadata()._sClassName === "sap.m.DatePicker") {
								y.setEnabled(bFlag);
							}
						});
				});
		},
		//Función que permite ocultar secciones del aplicativo.
		mostrarSecciones: function () {
			var info = this._getCore().byId("Cinformacion").getSelected();
			var comb = this._getCore().byId("Ccombustible").getSelected();
			var lubr = this._getCore().byId("Clubricantes").getSelected();
			var desg = this._getCore().byId("Cdesgaste").getSelected();
			var acti = this._getCore().byId("Cactividades").getSelected();
			var falla = this._getCore().byId("Cfallas").getSelected();
			var aprob = this._getCore().byId("Caprobacion").getSelected();

			var info1 = this.getView().byId("Sinformacion");
			var comb1 = this.getView().byId("Scombustible");
			var lubr1 = this.getView().byId("Slubricantes");
			var desg1 = this.getView().byId("Sdesgaste");
			var acti1 = this.getView().byId("Sactividades");
			var falla1 = this.getView().byId("Sfallas");
			var aprob1 = this.getView().byId("Saprobacion");

			if (info) {
				info1.setVisible(false);
			} else {
				info1.setVisible(true);
			}
			if (comb) {
				comb1.setVisible(false);
			} else {
				comb1.setVisible(true);
			}
			if (lubr) {
				lubr1.setVisible(false);
			} else {
				lubr1.setVisible(true);
			}
			if (desg) {
				desg1.setVisible(false);
			} else {
				desg1.setVisible(true);
			}
			if (acti) {
				acti1.setVisible(false);
			} else {
				acti1.setVisible(true);
			}
			if (falla) {
				falla1.setVisible(false);
			} else {
				falla1.setVisible(true);
			}
			if (aprob) {
				aprob1.setVisible(false);
			} else {
				aprob1.setVisible(true);
			}
			this._closeSec();
		},
		//Formateo de fecha.
		formateDate: function (fecha) {
			var year = fecha.getFullYear();
			var month = fecha.getMonth() + 1;
			var day = fecha.getDate();

			if ((day + "").length === 1) {

				day = "0" + day;

			}
			if ((month + "").length === 1) {

				month = "0" + month;

			}
			fecha = day + "/" + (month) + "/" + year;
			return fecha;
		},
		//Formateo de fecha.
		formateDate1: function (fecha) {
			var year = fecha.getFullYear();
			var month = fecha.getMonth() + 1;
			var day = fecha.getDate();
			day = day - 1;
			if ((day + "").length === 1) {

				day = "0" + day;

			}
			if ((month + "").length === 1) {

				month = "0" + month;

			}
			fecha = day + "/" + (month) + "/" + year;
			return fecha;
		},
		//Consulta de peajes de acuerdo a la estación seleccionada.
		asignarPeaje: function () {
			var oModel = this.getView().getModel();
			var estacion = oModel.getProperty("/peaje/ESTACION_PEAJE");
			var maestroPeaje = [];
			var filtro1 = "$filter= ID_PEAJE eq " + estacion + "";
			this.oDataModel.hcp.read("T_PEAJES?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						maestroPeaje = oData.results;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/combosData/maestroPeaje", maestroPeaje);
		},
		//Consultar todos los peajes asociados.
		leerPeajes: function () {
			var oModel = this.getView().getModel();
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			var mq_consecutivo = oModel.getProperty("/MQ_CONSECUTIVO");
			var id_actividad = oModel.getProperty("/id_actividad");
			var data = [];
			var that = this;
			var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
				pattern: "HH:mm:ss"
			});
			var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
			var filtro1 = "$filter= ID_EQUIPO eq '" + id_equipo + "' and MQ_CONSECUTIVO eq '" + mq_consecutivo + "' and ID_ACTIVIDAD eq " +
				id_actividad + "";
			this.oDataModel.hcp.read("T_DETALLE_PEAJE?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							var descripcion;
							var filtro1 = "$filter= ID_PEAJE eq " + oData.results[i].ESTACION_PEAJE + " and CATEGORIA eq '" + oData.results[i].CATEGORIA +
								"'";
							that.oDataModel.hcp.read("T_PEAJES?" + filtro1, null, null, false,
								function (oData) {
									descripcion = oData.results[0].DESCRIPCION;
								},
								function (oResponse) {
									console.log(oResponse);
								});
							var descripcionTipo;
							var filtro = "$filter= ID_TIPO eq " + oData.results[i].TIPO_PEAJE + "";
							that.oDataModel.hcp.read("T_TIPO_PEAJE?" + filtro, null, null, false,
								function (oData) {
									descripcionTipo = oData.results[0].DESCRIPCION;
								},
								function (oResponse) {
									console.log(oResponse);
								});
							var hora;
							if (oData.results[i].HORA) {
								if (oData.results[i].HORA.ms) {
									hora = timeFormat.format(new Date(oData.results[i].HORA.ms + TZOffsetMs));
								} else {
									hora = "";
								}
							}
							var arreglo = {
								estacion: descripcion,
								hora: hora,
								tipo_peaje: descripcionTipo,
								valor: oData.results[i].VALOR
							};
							data.push(arreglo);
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/infoPeaje", data);
		},
		//Asignar categoría de peaje automaticamente.
		asignarCategoria: function () {
			var oModel = this.getView().getModel();
			var categoria = oModel.getProperty("/peaje/CATEGORIA");
			oModel.setProperty("/peaje/VALOR", categoria);
			oModel.setProperty("/peaje/MATERIAL_SAP", categoria);
		},
		//Validación para Combustible de la cantidad máxima.
		validarCantidad: function (oEvent) {
			var oModel = this.getView().getModel();
			var oItem = oEvent.getSource(),
				cantidad = parseFloat(oItem.getValue()),
				descripcion = oItem.getParent().getCells()[0].getSelectedItem().getText();
			var material = oModel.getProperty("/material");
			var filtro1 = "$filter= ID_MATERIALES eq '" + material + "' and TIPO_CONSUMO eq 'COMBUSTIBLE' and DESCRIPCION eq '" + descripcion +
				"'";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						oModel.setProperty("/cantidad_max", oData.results[0].CANTIDAD_MAXIMA);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			var cantidadMax = parseFloat(oModel.getProperty("/cantidad_max"));
			if (cantidadMax) {
				if (cantidad > cantidadMax) {
					sap.m.MessageBox.error("La cantidad digitada supera la cantidad máxima");
					oItem.setValue(0);
				}
			}
		},
		//Validación para Lubricante de la cantidad máxima.
		validarCantidad_lubr: function (oEvent) {
			var oModel = this.getView().getModel();
			var oItem = oEvent.getSource(),
				cantidad = parseFloat(oItem.getValue()),
				descripcion = oItem.getParent().getCells()[0].getSelectedItem().getText();
			var material = oModel.getProperty("/material");
			var filtro1 = "$filter= ID_MATERIALES eq '" + material + "' and TIPO_CONSUMO eq 'LUBRICANTES' and DESCRIPCION eq '" + descripcion +
				"'";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						oModel.setProperty("/cantidad_max_lubr", oData.results[0].CANTIDAD_MAXIMA);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			var cantidadMax = parseFloat(oModel.getProperty("/cantidad_max_lubr"));
			if (cantidadMax) {
				if (cantidad > cantidadMax) {
					sap.m.MessageBox.error("La cantidad digitada supera la cantidad máxima");
					oItem.setValue(0);
				}
			}
		},
		//Validación para Desgaste de la cantidad máxima.
		validarCantidad_desg: function (oEvent) {
			var oModel = this.getView().getModel();
			var oItem = oEvent.getSource(),
				cantidad = parseFloat(oItem.getValue()),
				descripcion = oItem.getParent().getCells()[0].getSelectedItem().getText();
			var material = oModel.getProperty("/material");
			var filtro1 = "$filter= ID_MATERIALES eq '" + material + "' and TIPO_CONSUMO eq 'DESGASTE' and DESCRIPCION eq '" + descripcion +
				"'";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro1, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						oModel.setProperty("/cantidad_max_desg", oData.results[0].CANTIDAD_MAXIMA);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			var cantidadMax = parseFloat(oModel.getProperty("/cantidad_max_desg"));
			if (cantidadMax) {
				if (cantidad > cantidadMax) {
					sap.m.MessageBox.error("La cantidad digitada supera la cantidad máxima");
					oItem.setValue(0);
				}
			}
		},
		//Validación para que el horometro se encuentre dentro del rango permitido.
		validarHorometroConsumo: function (oEvent) {
			var oModel = this.getView().getModel();
			var oItem = oEvent.getSource(),
				cantidad = parseFloat(oItem.getValue());
			var cantidadMax = oModel.getProperty("/informacion_general/HOROMETRO_FINAL");
			var cantidadMin = oModel.getProperty("/informacion_general/HOROMETRO_INICIAL");
			if (cantidad > cantidadMax || cantidad < cantidadMin) {
				sap.m.MessageBox.error("El valor de Horómetro no se encuentra dentro del rango permitido.");
				oItem.setValue(0);
			}
		},
		//Validación para que el odómetro se encuentre dentro del rango permitido.
		validarOdometroConsumo: function (oEvent) {
			var oModel = this.getView().getModel();
			var oItem = oEvent.getSource(),
				cantidad = parseFloat(oItem.getValue());
			var cantidadMax = oModel.getProperty("/informacion_general/KILOMETRO_FINAL");
			var cantidadMin = oModel.getProperty("/informacion_general/KILOMETRO_INICIAL");
			if (cantidad > cantidadMax || cantidad < cantidadMin) {
				sap.m.MessageBox.error("El valor de Odómetro no se encuentra dentro del rango permitido.");
				oItem.setValue(0);
			}
		},
		//Esconder o mostrar la vista de equipos.
		getSplitAppObj: function () {
			var result = this.getView().byId("pagina_master");
			if (!result) {
				jQuery.sap.log.info("No lo encontró");
			}
			return result;
		},
		//Esconder o mostrar la vista de equipos.
		mostrarMaster: function () {
			//this.getSplitAppObj().setMode("ShowHideMode"); 
			this.getSplitAppObj().setMode("HideMode");
		},
		//Asignar unidad automaticamente en materiales de consumo.
		asignarUnidad: function (oEvent) {
			var oModel = this.getView().getModel();
			var oItem = oEvent.getSource(),
				id_consumo = oItem.getSelectedKey(),
				unidad;
			var that = this;
			var filtro = "$filter= ID eq " + id_consumo + "";
			this.oDataModel.hcp.read("T_MATERIALES_CONSUMO?" + filtro, null, null, false,
				function (oData) {
					unidad = oData.results[0].UNIDAD_MEDIDA;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oItem.getParent().getCells()[2].setValue(unidad);
		},
		//fin funciones para cargar información.

		//Funciones de Guardar y navegabilidad.
		//Abre la ventana emergente de equipos consultados.
		addFuel: function () {
			this._openFuel();
		},
		//Abre la ventana emergente de equipos consultados.
		_openFuel: function () {
			// validate dialog lazily
			if (!this._oFuel) {
				// create dialog via fragment factory
				this._oFuel = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.fuel", this);
				this._getView().addDependent(this._oFuel);
			}
			this._oFuel.open();
		},
		//Cierra la ventana emergente de equipos consultados.
		_closeFuel: function () {
			this._oFuel.close();
		},
		
		//Abre la ventana emergente para seleccionar la fecha y operador al crear el MQ.
		_openDate: function (id_equipo, oModel) {
			// validate dialog lazily
			if (!this._fecha) {
				// create dialog via fragment factory
				this._fecha = sap.ui.xmlfragment("idFecha", "mqa.ui5.mq.view.fragment.fecha", this);
				this._getView().addDependent(this._fecha);
			}
			
			var formFest = sap.ui.core.Fragment.byId("idFecha", "idFestivo");
			formFest.setVisible(false);
			
			//Fecha Último MQ - Equipo
			var maxDate;
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern: "YYYY-MM-dd"});
			var filtro = "?MaxMQ=" + "'" + id_equipo + "'";
			
			jQuery.ajax({
				url: this.hanadb + "/xs_service/mq_director_report.xsjs" + filtro, 
				method: "GET",
				contentType: "application/json",
				async: false,
				success: function (data) {
					Util._showBI(false);
					
					if (data.code === 1) {
					    maxDate = data.data[0].FECHA;
					    oModel.setProperty("/mDataFest", data.data[0]);
					    
					} else {
						sap.m.MessageBox.error(data.message, "error");
					}
					
				},
				error: function (err) {
					sap.m.MessageBox.error(err, "error");
				}
			});
			
			if (maxDate){
			    var fechaMax = maxDate.split("T");
			    
			    maxDate = new Date(fechaMax[0]);
			    maxDate.setDate(maxDate.getDate() + 2);
			    
			    maxDate = dateFormat.format(maxDate);
			    
			    var fechaMQ = sap.ui.core.Fragment.byId("idFecha", "fechamq");
			    fechaMQ.setValue(maxDate);
			    fechaMQ.setEditable(false);
			}
			
			this._fecha.open();
		},
		
		//Cierra la ventana emergente para seleccionar la fecha y operador al crear el MQ.
		_closeDate: function () {
			this._fecha.close();
		},
		//Abre ventana con la estructura de los elementos pep.
		_openPep: function () {
			// validate dialog lazily
			if (!this._pep) {
				// create dialog via fragment factory
				this._pep = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.estructurapep", this);
				this._getView().addDependent(this._pep);
			}
			this._pep.open();
		},
		//Cierra ventana con la estructura de los elementos pep.
		_closePep: function () {
			this._pep.close();
		},
		//Abre ventana para ocultar o mostrar secciones del MQ.
		_openSecc: function () {
			// validate dialog lazily
			if (!this._sec) {
				// create dialog via fragment factory
				this._sec = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.secciones", this);
				this._getView().addDependent(this._sec);
			}
			this._sec.open();
		},
		//Cierra ventana para ocultar o mostrar secciones del MQ.
		_closeSec: function () {
			this._sec.close();
		},
		//Agregar registro en la tabla de Lubricante.
		agregarLubricante: function () {
			var oModel = this.getView().getModel();
			var arregloLubricantes = oModel.getProperty("/lubricants");
			var lubricante = {
				ID_LUBRICANTE: 1,
				ID_EQUIPO: "",
				LUBRICANTE: 0,
				CANTIDAD: 0,
				UNIDAD: "",
				VALOR_HOROMETRO: 0,
				VALOR_ODOMETRO: 0,
				NUMERO_VALE: 0,
				ALMACEN: null,
				OBSERVACIONES: "",
				MQ_CONSECUTIVO: ""
			};
			arregloLubricantes.push(lubricante);
			oModel.setProperty("/lubricants", arregloLubricantes);
		},
		//Agregar registro en la tabla de Desgaste.
		agregarDesgaste: function () {
			var oModel = this.getView().getModel();
			var arregloDesgaste = oModel.getProperty("/wear");
			var desgaste = {
				ID_DESGASTE: 1,
				ID_EQUIPO: "",
				ELEMENTO: "",
				CANTIDAD: 0,
				UNIDAD: "",
				VALOR_HOROMETRO: 0,
				VALOR_ODOMETRO: 0,
				NUMERO_VALE: 0,
				ALMACEN: null,
				OBSERVACIONES: "",
				MQ_CONSECUTIVO: ""
			};
			arregloDesgaste.push(desgaste);
			oModel.setProperty("/wear", arregloDesgaste);
		},
		//Agregar registro en la tabla de Fallas.
		agregarFallas: function () {
			var oModel = this.getView().getModel();
			var arregloFalla = oModel.getProperty("/fault");
			var fallas = {
				ID_FALLA: 1,
				ID_EQUIPO: "",
				PARTE_OBJETO: "",
				CLASE_INCIDENTE: "",
				INCIDENTE: "",
				FECHA_INCIDENTE: new Date(),
				OBSERVACIONES: "",
				MQ_CONSECUTIVO: ""
			};
			arregloFalla.push(fallas);
			oModel.setProperty("/fault", arregloFalla);
		},
		//Agregar registro en la tabla de Combustible.
		agregarCombustible: function () {
			var oModel = this.getView().getModel();
			var arregloCombustible = oModel.getProperty("/fuels");
			var combustible = {
				ID_COMBUSTIBLE: 1,
				ID_EQUIPO: "",
				COMBUSTIBLE: 0,
				CANTIDAD: 0,
				UNIDAD: "",
				VALOR_HOROMETRO: 0,
				NUMERO_VALE: 0,
				VALOR_ODOMETRO: 0,
				ALMACEN: null,
				OBSERVACIONES: "",
				MQ_CONSECUTIVO: "",
				TIPO_COMBUSTIBLE: null,
				//TANQUEO_FULL: "",
			};
			arregloCombustible.push(combustible);
			oModel.setProperty("/fuels", arregloCombustible);
		},
		//Borra registro en la tabla de Combustible.
		borrarCombustible: function (oEvent) {
			var oModel = this.getView().getModel();
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "SIN LIBERAR" || oModel.getProperty(
					"/aprobaciones/ESTADO_APROBACION") ===
				"RECHAZADO" || oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "") {
				var atipoConcepto = oModel.getProperty("/fuels");
				var id = oEvent.getSource().getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().getPath()).ID_COMBUSTIBLE;
				var oItem = oEvent.getParameter("listItem");
				var sPath = oItem.getBindingContext().getPath();
				var iItemIndex = sPath.split("/")[sPath.split("/").length - 1];
				atipoConcepto.splice(iItemIndex, 1);
				var sTableName = "T_MQ_COMBUSTIBLE",
					sFieldName = "ELIMINAR",
					valueToUpdate = "X",
					sFieldKey = "ID_COMBUSTIBLE",
					sFieldKeyValue = id;
				this.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
				oModel.setProperty("/fuels", atipoConcepto);
			} else {
				sap.m.MessageBox.error("No puede Eliminar el registro");
			}
		},
		//Borra registro en la tabla de Lubricante.
		borrarLubricante: function (oEvent) {
			var oModel = this.getView().getModel();
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "SIN LIBERAR" || oModel.getProperty(
					"/aprobaciones/ESTADO_APROBACION") ===
				"RECHAZADO" || oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "") {
				var atipoConcepto = oModel.getProperty("/lubricants");
				var id = oEvent.getSource().getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().getPath()).ID_LUBRICANTE;
				var oItem = oEvent.getParameter("listItem");
				var sPath = oItem.getBindingContext().getPath();

				var iItemIndex = sPath.split("/")[sPath.split("/").length - 1];
				atipoConcepto.splice(iItemIndex, 1);
				var sTableName = "T_MQ_LUBRICANTES",
					sFieldName = "ELIMINAR",
					valueToUpdate = "X",
					sFieldKey = "ID_LUBRICANTE",
					sFieldKeyValue = id;
				this.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
				oModel.setProperty("/lubricants", atipoConcepto);
			} else {
				sap.m.MessageBox.error("No puede Eliminar el registro");
			}
		},
		//Borra registro en la tabla de Desgaste.
		borrarDesgaste: function (oEvent) {
			var oModel = this.getView().getModel();
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "SIN LIBERAR" || oModel.getProperty(
					"/aprobaciones/ESTADO_APROBACION") ===
				"RECHAZADO" || oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "") {
				var atipoConcepto = oModel.getProperty("/wear");
				var id = oEvent.getSource().getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().getPath()).ID_DESGASTE;
				var oItem = oEvent.getParameter("listItem");
				var sPath = oItem.getBindingContext().getPath();
				var iItemIndex = sPath.split("/")[sPath.split("/").length - 1];
				atipoConcepto.splice(iItemIndex, 1);
				var sTableName = "T_MQ_DESGASTE",
					sFieldName = "ELIMINAR",
					valueToUpdate = "X",
					sFieldKey = "ID_DESGASTE",
					sFieldKeyValue = id;
				this.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
				oModel.setProperty("/wear", atipoConcepto);
			} else {
				sap.m.MessageBox.error("No puede Eliminar el registro");
			}
		},
		//Borra registro en la tabla de Fallas.
		borrarFallas: function (oEvent) {
			var oModel = this.getView().getModel();
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "SIN LIBERAR" || oModel.getProperty(
					"/aprobaciones/ESTADO_APROBACION") ===
				"RECHAZADO" || oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "") {
				var oModel = this.getView().getModel();
				var atipoConcepto = oModel.getProperty("/fault");
				var id = oEvent.getSource().getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().getPath()).ID_FALLA;
				var oItem = oEvent.getParameter("listItem");
				var sPath = oItem.getBindingContext().getPath();
				var iItemIndex = sPath.split("/")[sPath.split("/").length - 1];
				atipoConcepto.splice(iItemIndex, 1);
				var sTableName = "T_MQ_FALLAS",
					sFieldName = "ELIMINAR",
					valueToUpdate = "X",
					sFieldKey = "ID_FALLA",
					sFieldKeyValue = id;
				this.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
				oModel.setProperty("/fault", atipoConcepto);
			} else {
				sap.m.MessageBox.error("No puede Eliminar el registro");
			}
		},
		//Borra registro en la tabla de Actividad.
		borrarActividad: function (oEvent) {
			var oModel = this.getView().getModel();
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "SIN LIBERAR" || oModel.getProperty(
					"/aprobaciones/ESTADO_APROBACION") ===
				"RECHAZADO" || oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "") {
				var atipoConcepto = oModel.getProperty("/activities");
				var id = oEvent.getSource().getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().getPath()).ID_ACTIVITY;
				var oItem = oEvent.getParameter("listItem");
				var sPath = oItem.getBindingContext().getPath();

				var iItemIndex = sPath.split("/")[sPath.split("/").length - 1];
				atipoConcepto.splice(iItemIndex, 1);
				var sTableName = "T_MQ_ACTIVIDAD",
					sFieldName = "ELIMINAR",
					valueToUpdate = "X",
					sFieldKey = "ID_ACTIVITY",
					sFieldKeyValue = id;
				this.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
				oModel.setProperty("/activities", atipoConcepto);
			} else {
				sap.m.MessageBox.error("No puede Eliminar el registro");
			}
		},
		eliminarRegistro: function (oEvent) {
			var oModel = this.getView().getModel();
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "SIN LIBERAR" || oModel.getProperty(
					"/aprobaciones/ESTADO_APROBACION") ===
				"RECHAZADO" || oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "") {
				var oData = oModel.getProperty("/activities");
				var index = oModel.getProperty("/id_registro");
				var id = oData[index].ID_ACTIVITY;
				oData.splice(index, 1);
				var sTableName = "T_MQ_ACTIVIDAD",
					sFieldName = "ELIMINAR",
					valueToUpdate = "X",
					sFieldKey = "ID_ACTIVITY",
					sFieldKeyValue = id;
				this.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
				oModel.setProperty("/activities", oData);
			} else {
				sap.m.MessageBox.error("No puede Eliminar el registro");
			}
		},
		//Función que permite actualizar un campo específico en una tabla de SCP.
		funcionEliminar: function (tabla, campo, valor, llave, valor_llave) {
			var oInfo = {
				sTableName: tabla,
				sFieldName: campo,
				valueToUpdate: valor,
				sFieldKey: llave,
				sFieldKeyValue: valor_llave
			};

			jQuery.ajax({
				url: "/HANA_DB/logical/xs_service/update_1_field.xsjs?P_TABLA=" + oInfo.sTableName + "&P_CAMPO=" + oInfo
					.sFieldName +
					"&P_VALOR=" + oInfo.valueToUpdate + "&P_CAMPOPK=" + oInfo.sFieldKey + "&P_VALORPK=" + oInfo.sFieldKeyValue,
				method: 'GET',
				dataType: 'json',
				async: false,
				success: function (result) {
					console.log(["Proceso ok en actualizacíon de motivo: ", result]);
				},
				error: function (err) {
					console.log(["Error actualizando el motivo: ", err]);
				}
			});
		},
		//Limpiar el modelo de Peaje.
		_saveFuel: function () {
			var oModel = this.getView().getModel();
			var oData = oModel.getProperty("/combustible");
			var modelo = oModel.getProperty("/fuels");
			var obj = {
				ID_COMBUSTIBLE: 1,
				ID_EQUIPO: "",
				COMBUSTIBLE: oData.COMBUSTIBLE,
				CANTIDAD: oData.CANTIDAD,
				VALOR_HOROMETRO: oData.VALOR_HOROMETRO,
				NUMERO_VALE: oData.NUMERO_VALE
			};
			modelo.push(obj);
			oModel.setProperty("/fuels", modelo);
			oModel.setProperty("/combustible", []);
			this._closeFuel();
		},
		//Validar tipo de campos.
		_value: function (sId, isRequire) {
			var empty = true;
			if (this._getCore().byId(sId).getMetadata()._sClassName === "sap.m.Input") {
				if (this._getCore().byId(sId).getValue() !== "" || this._getCore().byId(sId).getValue() !== null) {
					empty = false;
				}
			} else if (this._getCore().byId(sId).getMetadata()._sClassName === "sap.m.Select") {
				if (this._getCore().byId(sId).getSelectedItem() !== null) {
					empty = false;
				}
			}
			this._getCore().byId(sId).setValueState("None");

			if (!empty && isRequire) {
				this._getCore().byId(sId).setValueState("Success");
			} else if (empty && isRequire) {
				this._getCore().byId(sId).setValueState("Error");
			}

			return empty;
		},
		//Guardar sección de Información General.
		guardarInfoG: function () {
			var oModel = this.getView().getModel();
			var data = oModel.getProperty("/informacion_general");
			data.ID_EQUIPO = this.ID_EQUIPO;
			data.MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
			if (!data.HOROMETRO_INICIAL) {
				data.HOROMETRO_INICIAL = 0;
			}
			if (!data.KILOMETRO_INICIAL) {
				data.KILOMETRO_INICIAL = 0;
			}
			if (data.ID_INFORMACION === 1) {
				this.oDataModel.hcp.create("/T_MQ_INFORMACION", data, {
					success: function () {
						//sap.m.MessageBox.success("Información creada correctamente");
					},
					error: function (err) {
						console.log(err);
					}
				});
			} else {
				this.oDataModel.hcp.update("/T_MQ_INFORMACION(" + data.ID_INFORMACION + ")", data, {
					success: function () {
						//sap.m.MessageBox.success("Información Actualizada correctamente");
					},
					error: function (err) {
						console.log(err);
					}
				});
			}
		},
		//Guardar sección de Lubricante.
		guardarLubricante: function () {
			var oModel = this.getView().getModel();
			var lubricantes = oModel.getProperty("/lubricants");
			var flag = false;
			var flag1 = false;
			this.oDataModel.hcp.setUseBatch(true);
			for (var i = 0; i < lubricantes.length; i++) {
				lubricantes[i].ID_EQUIPO = this.ID_EQUIPO;
				lubricantes[i].MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
				if (lubricantes[i].ID_LUBRICANTE === 1) {
					this.oDataModel.hcp.create("/T_MQ_LUBRICANTES", lubricantes[i], {
						success: function () {
							flag = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				} else {
					this.oDataModel.hcp.update("/T_MQ_LUBRICANTES(" + lubricantes[i].ID_LUBRICANTE + ")", lubricantes[i], {
						success: function () {
							flag1 = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				}
			}
			this.oDataModel.hcp.submitChanges();
			this.oDataModel.hcp.setUseBatch(false);
			if (flag) {
				//sap.m.MessageBox.success("Lubricantes creado correctamente");
			}
			if (flag1) {
				//sap.m.MessageBox.success("Lubricantes Actualizado correctamente");
			}
		},
		//Guardar sección de Combustible.
		guardarCombustible: function () {
			var oModel = this.getView().getModel();
			var combustible = oModel.getProperty("/fuels");
			var flag = false;
			var flag1 = false;
			this.oDataModel.hcp.setUseBatch(true);
			for (var i = 0; i < combustible.length; i++) {
				combustible[i].ID_EQUIPO = this.ID_EQUIPO;
				combustible[i].MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
				if (combustible[i].ID_COMBUSTIBLE === 1) {
					this.oDataModel.hcp.create("/T_MQ_COMBUSTIBLE", combustible[i], {
						success: function () {
							flag = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				} else {
					for (var i = 0; i < combustible.length; i++) {
						this.oDataModel.hcp.update("/T_MQ_COMBUSTIBLE(" + combustible[i].ID_COMBUSTIBLE + ")", combustible[i], {
							success: function () {
								flag1 = true;
							},
							error: function (err) {
								console.log(err);
							}
						});
					}
				}
			}
			this.oDataModel.hcp.submitChanges();
			this.oDataModel.hcp.setUseBatch(false);
			if (flag) {
				//sap.m.MessageBox.success("Combustible Creado correctamente");
			}
			if (flag1) {
				//sap.m.MessageBox.success("Combustible Actualizado correctamente");
			}
		},
		//Guardar sección de Desgaste.
		guardarDesgaste: function () {
			var oModel = this.getView().getModel();
			var desgaste = oModel.getProperty("/wear");
			var flag = false;
			var flag1 = false;
			this.oDataModel.hcp.setUseBatch(true);
			for (var i = 0; i < desgaste.length; i++) {
				desgaste[i].ID_EQUIPO = this.ID_EQUIPO;
				desgaste[i].MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
				if (desgaste[i].ID_DESGASTE === 1) {
					this.oDataModel.hcp.create("/T_MQ_DESGASTE", desgaste[i], {
						success: function () {
							flag = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				} else {
					this.oDataModel.hcp.update("/T_MQ_DESGASTE(" + desgaste[i].ID_DESGASTE + ")", desgaste[i], {
						success: function () {
							flag1 = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				}
			}
			this.oDataModel.hcp.submitChanges();
			this.oDataModel.hcp.setUseBatch(false);
			if (flag) {
				//sap.m.MessageBox.success("Desgaste Creado correctamente");
			}
			if (flag1) {
				//sap.m.MessageBox.success("Desgaste Actualizado correctamente");
			}
		},
		//Guardar sección de Fallas.
		guardarFallas: function () {
			var oModel = this.getView().getModel();
			var fallas = oModel.getProperty("/fault");
			var flag = false;
			var flag1 = false;
			this.oDataModel.hcp.setUseBatch(true);
			for (var i = 0; i < fallas.length; i++) {
				fallas[i].ID_EQUIPO = this.ID_EQUIPO;
				fallas[i].MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
				if (fallas[i].ID_FALLA === 1) {
					this.oDataModel.hcp.create("/T_MQ_FALLAS", fallas[i], {
						success: function () {
							flag = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				} else {
					this.oDataModel.hcp.update("/T_MQ_FALLAS(" + fallas[i].ID_FALLA + ")", fallas[i], {
						success: function () {
							flag1 = true;
						},
						error: function (err) {
							console.log(err);
						}
					});
				}
			}
			this.oDataModel.hcp.submitChanges();
			this.oDataModel.hcp.setUseBatch(false);
			if (flag) {
				//sap.m.MessageBox.success("Fallas Creado correctamente");
			}
			if (flag1) {
				//sap.m.MessageBox.success("Fallas Actualizado correctamente");
			}
		},
		//Guardar sección de Aprobación.
		guardarAprobacion: function () {
			var oModel = this.getView().getModel();
			var aprobacion = oModel.getProperty("/aprobaciones");
			aprobacion.ID_EQUIPO = this.ID_EQUIPO;
			aprobacion.FECHA_APROBACION = new Date(aprobacion.FECHA_APROBACION);
			aprobacion.MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
			if (!aprobacion.HORA) {
				aprobacion.HORA = null;
			}
			if (aprobacion.ID_APROBACION === 1) {
				this.oDataModel.hcp.create("/T_MQ_APROBACION", aprobacion, {
					success: function () {
						//sap.m.MessageBox.success("Aprobación Creada correctamente");
					},
					error: function (err) {
						console.log(err);
					}
				});
			} else {
				this.oDataModel.hcp.update("/T_MQ_APROBACION(" + aprobacion.ID_APROBACION + ")", aprobacion, {
					success: function () {
						//sap.m.MessageBox.success("Aprobación Actualizada correctamente");
					},
					error: function (err) {
						console.log(err);
					}
				});
			}
		},
		//Guardar sección de Peaje.
		guardarPeaje: function () {
			var oModel = this.getView().getModel();
			var oPeaje = oModel.getProperty("/peaje");
			oPeaje.VALOR = sap.ui.getCore().byId("valorPeaje").getValue();
			oPeaje.MATERIAL_SAP = sap.ui.getCore().byId("matPeaje").getValue();
			oPeaje.VALOR = parseFloat(oPeaje.VALOR);
			oPeaje.MQ_CONSECUTIVO = this.getView().getModel().getProperty("/MQ_CONSECUTIVO");
			oPeaje.ID_EQUIPO = this.getView().getModel().getProperty("/ID_EQUIPO");
			oPeaje.ID_ACTIVIDAD = this.getView().getModel().getProperty("/id_actividad");
			var modelo = [];
			modelo.push(oPeaje);
			modelo = JSON.stringify(modelo);
			//oPeaje.HORA = new Date(timeFormat.parse(timeStr).getTime());
			jQuery.ajax({
				url: "/HANA_DB/logical/xs_service/sincronizar_data.xsjs",
				method: "POST",
				async: false,
				data: modelo,
				success: function (oResponse) {
					console.log(oResponse);
					sap.m.MessageBox.success("Peaje guardado correctamente");

				},
				error: function (oError) {
					console.log(oError);
				}
			});
			//Convertir a Json el array.
			modelo = JSON.parse(modelo);
			this.leerPeajes();
			this._closeToll();
			//oModel.setProperty("/peaje", []);
		},
		//Guardar Todas las secciones del MQ.
		guardar: function () {
			var model = this.getView().getModel();
			var flag = this.validarCampos();
			if (flag) {
				return;
			}
			if (this.getView().getModel().getProperty("/MQ_CONSECUTIVO") !== "") {
				try {
					this.guardarInfoG();
					this.guardarLubricante();
					this.guardarCombustible();
					this.guardarDesgaste();
					this.guardarFallas();
					this.guardarAprobacion();
					this.guardarActividad();
					sap.m.MessageBox.success("MQ Guardado correctamente");
					this.setModelEmpty();
				} catch (error) {
					console.log(error);
				}
			} else {
				sap.m.MessageBox.error("Debe crear el MQ antes de guardar.");
			}

		},
		//Función para validar campos obligatorios.
		validarCampos: function () {
			var model = this.getView().getModel();
			var flag = false;
			//Odómetros.
			/*if (model.getProperty("/informacion_general/KILOMETRO_INICIAL") === "0" || !model.getProperty(
					"/informacion_general/KILOMETRO_INICIAL")) {
				sap.m.MessageToast.show("Los datos de Odómetro no son válidos.", {
					duration: 20000
				});
				model.setProperty("/validate/kilometro_inicial", "Error");
				flag = true;
				return flag;
			} else {
				model.setProperty("/validate/kilometro_inicial", "None");
			}*/
			if (model.getProperty("/informacion_general/KILOMETRO_FINAL") === "0" || !model.getProperty(
					"/informacion_general/KILOMETRO_FINAL")) {
				sap.m.MessageToast.show("Los datos de Odómetro no son válidos.", {
					duration: 20000
				});
				model.setProperty("/validate/kilometro_final", "Error");
				flag = true;
				return flag;
			} else {
				model.setProperty("/validate/kilometro_final", "None");
			}
			//Horómetros.
			/*if (model.getProperty("/informacion_general/HOROMETRO_INICIAL") === "0" || !model.getProperty(
					"/informacion_general/HOROMETRO_INICIAL")) {
				sap.m.MessageToast.show("Los datos de Horómetro no son válidos.", {
					duration: 20000
				});
				model.setProperty("/validate/horometro_inicial", "Error");
				flag = true;
				return flag;
			} else {
				model.setProperty("/validate/horometro_inicial", "None");
			}*/
			if (model.getProperty("/informacion_general/HOROMETRO_FINAL") === "0" || !model.getProperty(
					"/informacion_general/HOROMETRO_FINAL")) {
				sap.m.MessageToast.show("Los datos de Horómetro no son válidos.", {
					duration: 20000
				});
				model.setProperty("/validate/horometro_final", "Error");
				flag = true;
				return flag;
			} else {
				model.setProperty("/validate/horometro_final", "None");
			}
			//Estado Aprobación.
			if (model.getProperty("/aprobaciones/ESTADO_APROBACION") === "" || !model.getProperty(
					"/aprobaciones/ESTADO_APROBACION")) {
				sap.m.MessageToast.show("El estado de Aprobación no puede estar vacío.", {
					duration: 20000
				});
				model.setProperty("/validate/estado_aprobacion", "Error");
				flag = true;
				return flag;
			} else {
				model.setProperty("/validate/estado_aprobacion", "None");
			}
			//Tipo Aprobación.
			if (model.getProperty("/aprobaciones/TIPO_APROBACION") === "" || !model.getProperty(
					"/aprobaciones/TIPO_APROBACION")) {
				sap.m.MessageToast.show("El tipo de Aprobación no puede estar vacío.", {
					duration: 20000
				});
				model.setProperty("/validate/tipo_aprobacion", "Error");
				flag = true;
				return flag;
			} else {
				model.setProperty("/validate/tipo_aprobacion", "None");
			}
			var combustible = model.getProperty("/fuels");
			if (combustible.length > 0) {
				var contador = 0;
				for (var i = 0; i < combustible.length; i++) {
					if (!combustible[i].VALOR_HOROMETRO || combustible[i].VALOR_HOROMETRO === 0) {
						sap.m.MessageToast.show("El valor de horómetro en combustible no es válido.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
					if (!combustible[i].VALOR_ODOMETRO || combustible[i].VALOR_ODOMETRO === 0) {
						sap.m.MessageToast.show("El valor de odómetro en combustible no es válido.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
					if (!combustible[i].NUMERO_VALE || combustible[i].NUMERO_VALE === 0) {
						sap.m.MessageToast.show("El número de Vale en combustible no es válido.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
					var dato = combustible.filter(function (Txt) {
						return Txt.COMBUSTIBLE === combustible[i].COMBUSTIBLE;
					});
					if (dato.length > 1) {
						sap.m.MessageToast.show("No puede haber duplicidad en el combustible para el Equipo", {
							duration: 20000
						});
						flag = true;
						return flag;
					}

				}
			}
			var lubricantes = model.getProperty("/lubricants");
			if (lubricantes.length > 0) {
				for (var i = 0; i < lubricantes.length; i++) {
					if (!lubricantes[i].NUMERO_VALE || lubricantes[i].NUMERO_VALE === 0) {
						sap.m.MessageToast.show("El número de vale en lubricantes no es válido.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
				}
			}
			var desgaste = model.getProperty("/wear");
			if (desgaste.length > 0) {
				for (var i = 0; i < desgaste.length; i++) {
					if (!desgaste[i].VALOR_HOROMETRO || desgaste[i].VALOR_HOROMETRO === 0) {
						sap.m.MessageToast.show("El valor de horómetro en desgaste no es válido.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
					if (!desgaste[i].VALOR_ODOMETRO || desgaste[i].VALOR_ODOMETRO === 0) {
						sap.m.MessageToast.show("El valor de odómetro en desgaste no es válido.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
					if (!desgaste[i].NUMERO_VALE || desgaste[i].NUMERO_VALE === 0) {
						sap.m.MessageToast.show("El No. de Control diario de salida en desgaste es obligatorio.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
					if (!desgaste[i].ALMACEN) {
						sap.m.MessageToast.show("El almacén es obligatorio diligenciarlo en desgaste.", {
							duration: 20000
						});
						flag = true;
						return flag;
					}
				}
			}
		},
		//Limpia el modelo actual del MQ.
		cancelarMq: function () {
			this.setModelEmpty();
			sap.m.MessageBox.error("Ha cancelado el proceso");
		},
		//Funciones de Llamado al ERP
		testERP: function () {
			this.oDataModel.erp.read("/ConsultaEquiposSet", null, null, false,
				function (oData) {
					console.log(oData);
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
		},
		//Función para envío de Datos al ERP PM.
		sincronizarErp: function () {
			var oModel = this.getView().getModel();
			var that = this;
			if (oModel.getProperty("/aprobaciones/ESTADO_APROBACION") === "APROBADO") {
				var id = oModel.getProperty("/informacion_general/ID_INFORMACION");
				var sincronizar = null;
				this.oDataModel.hcp.read("/T_PARAMETROS_APP", null, null, false,
					function (oData) {
						sincronizar = oData.results[0].SYNC_CONTADORES;
					},
					function (oResponse) {
						console.log(oResponse);
					});
				var horometro;
				var odometro;
				var contador;
				var filtro = "$filter= ID_EQUIPO eq '" + oModel.getProperty("/ID_EQUIPO") + "'";
				this.oDataModel.hcp.read("/T_EQUIPO?" + filtro, null, null, false,
					function (oData) {
						horometro = oData.results[0].HOROMETRO;
						odometro = oData.results[0].ODOMETRO;
						contador = oData.results[0].CONTADOR;
					},
					function (oResponse) {
						console.log(oResponse);
					});
				var contadorHorometro;
				var contadorOdometro;
				var filtro = "$filter= ID_INFORMACION eq " + id + "";
				this.oDataModel.hcp.read("/T_MQ_INFORMACION?" + filtro, null, null, false,
					function (oData) {
						contadorHorometro = oData.results[0].CONTADOR_HOROMETRO;
						contadorOdometro = oData.results[0].CONTADOR_ODOMETRO;
					},
					function (oResponse) {
						console.log(oResponse);
					});
				var combustible = oModel.getProperty("/fuels");
				for (var i = 0; i < combustible.length; i++) {
					if (oModel.getProperty("/fuels/" + i + "/TIPO_COMBUSTIBLE") === 3) {
						if (!contador) {
							contador = "";
						}
						var d = {
							Mdtxt: oModel.getProperty("/MQ_CONSECUTIVO"),
							Datum: oModel.getProperty("/informacion_general/FECHA_MQ"),
							Point: contador,
							Recdc: oModel.getProperty("/fuels/" + i + "/CANTIDAD")
						};

						this.oDataModel.erp.create("/ContadoresEquiposSet", d, {
							success: function (oResponse) {
								sap.m.MessageBox.success("Contadores Equipos " + oResponse.Messa);
								/*sap.m.MessageToast.show("Contadores Equipos " + oResponse.Messa, {
									duration: 20000
								});*/
								console.log(oResponse);
							},
							error: function (err) {
								console.log(err);
							}
						});
						if (odometro) {
							if (oModel.getProperty("/fuels/" + i + "/VALOR_ODOMETRO") !== 0 && oModel.getProperty("/fuels/" + i + "/VALOR_ODOMETRO")) {
								var d = {
									Mdtxt: oModel.getProperty("/MQ_CONSECUTIVO"),
									Datum: oModel.getProperty("/informacion_general/FECHA_MQ"),
									Point: odometro,
									Recdc: oModel.getProperty("/fuels/" + i + "/VALOR_ODOMETRO")
								};
								this.oDataModel.erp.create("/ContadoresEquiposSet", d, {
									success: function (oResponse) {
										/*	sap.m.MessageToast.show("Contadores Equipos " + oResponse.Messa, {
												duration: 20000
											});*/
										sap.m.MessageBox.success("Contadores Equipos " + oResponse.Messa);
										console.log(oResponse);
									},
									error: function (err) {
										console.log(err);
									}
								});

							} else {
								sap.m.MessageBox.error("El campo Odómetro es Obligatorio en el Combustible");
								return;
							}
						}
						if (horometro) {
							if (oModel.getProperty("/fuels/" + i + "/VALOR_HOROMETRO") !== 0 && oModel.getProperty("/fuels/" + i + "/VALOR_HOROMETRO")) {
								var d = {
									Mdtxt: oModel.getProperty("/MQ_CONSECUTIVO"),
									Datum: oModel.getProperty("/informacion_general/FECHA_MQ"),
									Point: horometro,
									Recdc: oModel.getProperty("/fuels/" + i + "/VALOR_HOROMETRO")
								};

								this.oDataModel.erp.create("/ContadoresEquiposSet", d, {
									success: function (oResponse) {
										if (isNaN(oResponse.Messa)) {

										} else {
											var sTableName = "T_MQ_INFORMACION",
												sFieldName = "CONTADOR_HOROMETRO",
												valueToUpdate = oResponse.Messa,
												sFieldKey = "ID_INFORMACION",
												sFieldKeyValue = id;
											that.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
										}
										sap.m.MessageBox.success("Contadores Equipos " + oResponse.Messa);
										/*sap.m.MessageToast.show("Contadores Equipos " + oResponse.Messa, {
											duration: 20000
										});*/
										console.log(oResponse);
									},
									error: function (err) {
										console.log(err);
									}
								});
							} else {
								sap.m.MessageBox.error("El campo Horómetro es Obligatorio en Combustible");
								return;
							}
						}
					}
				}
				if (!sincronizar) {
					if (odometro) {
						if (!contadorOdometro) {
							if (oModel.getProperty("/informacion_general/KILOMETRO_FINAL") !== 0) {
								var d = {
									Mdtxt: oModel.getProperty("/MQ_CONSECUTIVO"),
									Datum: oModel.getProperty("/informacion_general/FECHA_MQ"),
									Point: odometro,
									Recdc: oModel.getProperty("/informacion_general/KILOMETRO_FINAL")
								};
								this.oDataModel.erp.create("/ContadoresEquiposSet", d, {
									success: function (oResponse) {
										if (isNaN(oResponse.Messa)) {

										} else {
											var sTableName = "T_MQ_INFORMACION",
												sFieldName = "CONTADOR_ODOMETRO",
												valueToUpdate = oResponse.Messa,
												sFieldKey = "ID_INFORMACION",
												sFieldKeyValue = id;
											that.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
										}
										sap.m.MessageBox.success("Contadores Equipos " + oResponse.Messa);
										/*sap.m.MessageToast.show("Contadores Equipos " + oResponse.Messa, {
											duration: 20000
										});*/
										console.log(oResponse);
									},
									error: function (err) {
										console.log(err);
									}
								});

							} else {
								sap.m.MessageBox.error("El campo Odómetro Inicial y Final son Obligatorios");
								return;
							}
						} else {
							sap.m.MessageBox.error("Ya se enviarion los contadores de Odómetro de este MQ");
						}
					}
					if (horometro) {
						if (!contadorHorometro) {
							if (oModel.getProperty("/informacion_general/HOROMETRO_FINAL") !== 0) {
								var d = {
									Mdtxt: oModel.getProperty("/MQ_CONSECUTIVO"),
									Datum: oModel.getProperty("/informacion_general/FECHA_MQ"),
									Point: horometro,
									Recdc: oModel.getProperty("/informacion_general/HOROMETRO_FINAL")
								};

								this.oDataModel.erp.create("/ContadoresEquiposSet", d, {
									success: function (oResponse) {
										if (isNaN(oResponse.Messa)) {

										} else {
											var sTableName = "T_MQ_INFORMACION",
												sFieldName = "CONTADOR_HOROMETRO",
												valueToUpdate = oResponse.Messa,
												sFieldKey = "ID_INFORMACION",
												sFieldKeyValue = id;
											that.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
										}
										sap.m.MessageBox.success("Contadores Equipos " + oResponse.Messa);
										/*	sap.m.MessageToast.show("Contadores Equipos " + oResponse.Messa, {
												duration: 20000
											});*/
										console.log(oResponse);
									},
									error: function (err) {
										console.log(err);
									}
								});
							} else {
								sap.m.MessageBox.error("El campo Horómetro Inicial y Final son Obligatorios");
								return;
							}
						} else {
							sap.m.MessageBox.error("Ya se enviarion los contadores de Horómetro de este MQ");
						}
					}
				}

				/*var fecha = "2018/06/02";
				fecha = new Date(fecha);

				//Tiempos de Equipos.
				if (oModel.getProperty("/GRAFO_ACT")) {
					var d = {
						Aufnr: oModel.getProperty("/GRAFO_ACT"), //Grafo ej:4000005
						Aplzl: oModel.getProperty("/OPERACION_ACT"), //operación "1"
						Sumnr: oModel.getProperty("/COD_ACT"), //actividad "3"
						Objid: "01A", //oModel.getProperty("/ID_EQUIPO"), //Id del equipo 01A
						Werks: "467A", // Centro 101A
						Ismnw: parseFloat(oModel.getProperty("/horas_trabajadas")), //Horas trabajadas
						Pernr: 0, //Id de la persona
						Budat: fecha, //fecha del MQ
						Isdd: fecha, //fecha del MQ
						Iedd: fecha, //fecha del MQ
						Learr: "", //Clase de Actividad 1009S
						Message: ""
							//Message: oModel.getProperty("/MQ_CONSECUTIVO")
					};
					//model.push(d);
					this.oDataModel.erp.create("/TiemposEquiposSet", d, {
						success: function (oResponse) {
							console.log(oResponse);
							sap.m.MessageToast.show("Creación de Tiempos: " + oResponse.Message + "", {
								duration: 20000
							});
						},
						error: function (err) {
							console.log(err);
						}
					});
				}

				//Entity de Reporte de Horas de Producción al ERP.
				var ordenes = oModel.getProperty("/activities");
				for (var i = 0; i < ordenes.length; i++) {
					if (ordenes[i].TIPO_IMPUTACION === "M") {
						var hora_inicio = this.formatoHoras(ordenes[i].HORA_INICIO);
						var hora_fin = this.formatoHoras(ordenes[i].HORA_FIN);
						var orden = ordenes[i].ORDEN;
						//if (oModel.getProperty("/AUFNR_PLANTA")) {
						var d = {
							//Aufnr: oModel.getProperty("/AUFNR_PLANTA"), //Orden "1000124"
							Aufnr: orden, //Orden de la planta
							Vornr: "",
							Yield: 0,
							Scrap: 0,
							Steus: "",
							Matnr: "",
							Bdmng: 0,
							Humed: 0,
							Objid: 0,
							Arbpl: oModel.getProperty("/ID_EQUIPO"), //id del equipo
							Hini: hora_inicio, //PT11H22M33S
							Hfin: hora_fin //PT11H22M33S
						};
						//model.push(d);
						this.oDataModel.erp.create("/ConsumoMaterialesSet", d, {
							success: function (oResponse) {
								console.log(oResponse);
								sap.m.MessageToast.show("Consumo de Materiales: " + oResponse.Message + "", {
									duration: 20000
								});
							},
							error: function (err) {
								console.log(err);
							}
						});
					}
				}*/

				//Entity de Creación Aviso
				var fallas = oModel.getProperty("/fault");
				for (var i = 0; i < fallas.length; i++) {
					var id_falla = oModel.getProperty("/fault/" + i + "/ID_FALLA");
					var avisofalla;
					var filtro = "$filter= ID_FALLA eq " + id_falla + "";
					this.oDataModel.hcp.read("/T_MQ_FALLAS?" + filtro, null, null, false,
						function (oData) {
							avisofalla = oData.results[0].AVISO_FALLA;
						},
						function (oResponse) {
							console.log(oResponse);
						});
					if (!avisofalla) {
						var codegroup = "";
						var coding = "";
						if (oModel.getProperty("/fault/" + i + "/CLASE_INCIDENTE")) {
							codegroup = "CCY";
							coding = oModel.getProperty("/fault/" + i + "/CLASE_INCIDENTE");
						}
						var d = {
							Equipment: oModel.getProperty("/ID_EQUIPO"),
							CodeGroup: codegroup,
							Coding: coding,
							Strmlfndate: oModel.getProperty("/fault/" + i + "/FECHA_INCIDENTE"),
							NotifDate: new Date(),
							DlCodegrp: "MYE",
							DlCode: oModel.getProperty("/fault/" + i + "/PARTE_OBJETO"),
							/*DlCodegrp: "M&E",
							DlCode: "OB01",*/
							DCodegrp: "MYE",
							DCode: oModel.getProperty("/fault/" + i + "/INCIDENTE"),
							/*	DCodegrp: "M&E",
								DCode: "SN02",*/
							ShortText: oModel.getProperty("/MQ_CONSECUTIVO"),
							Observacion: oModel.getProperty("/fault/" + i + "/OBSERVACIONES")
						};
						//model.push(d);
						this.oDataModel.erp.create("/CreaAvisoSet", d, {
							success: function (oResponse) {
								console.log(oResponse);
								if (isNaN(oResponse.ShortText)) {

								} else {
									var sTableName = "T_MQ_FALLAS",
										sFieldName = "AVISO_FALLA",
										valueToUpdate = oResponse.ShortText,
										sFieldKey = "ID_FALLA",
										sFieldKeyValue = id_falla;
									that.funcionEliminar(sTableName, sFieldName, valueToUpdate, sFieldKey, sFieldKeyValue);
								}
								sap.m.MessageBox.success("Creación de Aviso " + oResponse.ShortText);
								/*	sap.m.MessageToast.show("Creación de Aviso " + oResponse.ShortText, {
										duration: 20000
									});*/
							},
							error: function (err) {
								console.log(err);
							}
						});
					} else {
						sap.m.MessageBox.error("Ya se encuentra creado el aviso para el MQ");
					}
				}
			} else {
				sap.m.MessageBox.error("No se escuentra aprobado el MQ");
			}

		},
		//Trae las plantas disponibles del ERP
		obtenerPlantas: function () {
			var oModel = this.getView().getModel();
			var model = [];
			this.oDataModel.erp.read("/ConsultaPlantasSet", null, null, false,
				function (oData) {
					oData.results.sort();
					console.log(oData.results);
					for (var i = 0; i < oData.results.length; i++) {
						var dato = model.filter(function (Txt) {
							return Txt.Name1 === oData.results[i].Name1;
						});
						if (dato.length === 0) {
							model.push(oData.results[i]);
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			oModel.setProperty("/combosData/plantas", model);
		},
		//Consulta para traer los proyectos del ERP.
		consultarCds: function () {
			var sField = "&$select=ProjectDescription,Project"
			var sFilter =
				"Version eq '' and (ConcatenatedActiveSystStsName eq 'REL' or ConcatenatedActiveSystStsName eq 'LIBP' and ConcatenatedActiveUserStsName eq 'RECO' or ConcatenatedActiveUserStsName eq 'PLAN' or ConcatenatedActiveUserStsName eq 'PROG' or  ConcatenatedActiveUserStsName eq 'VALI' or ConcatenatedActiveUserStsName eq 'EJEC')";
			sFilter += sField;
			var aPaths = "/combosData/proyectos";
			this.useReadOmodel("ZXC_PROJECTWITHVERSION_CDS/ZXC_PROJECTWITHVERSION", sFilter, aPaths, "s4h");
		},
		//Consulta para traer los tramos (elemento pep).
		consultarPep: function (oEvent) {
			var oModel = this.getView().getModel();
			var bNeedLoadWBS = oModel.getProperty("/bNeedLoadWBS");
			try {
				var sOrder = oEvent.getSource().data("order");
				if (sOrder === "getWBS") {
					bNeedLoadWBS = true;
				}
			} catch (err) {
				jQuery.sap.log.info("El evento no proviene del cmb de actividades. Se omite la validación de elementos PEP.");
			}
			this.setearAbscisas();
			if (this._getCore().byId("proyect")) {
				var proyecto = this._getCore().byId("proyect").getSelectedKey();
			} else {
				var proyecto = oModel.getProperty("/activities/0/PROYECTO");
			}
			if (proyecto === "") {
				var proyecto = oModel.getProperty("/activities/0/PROYECTO");
			}
			//debugger;
			var filtro = "$filter= Version eq '' and Project eq '" + proyecto + "'&$select=WBSElement,WBSDescription";
			var model = [];
			var datos = oModel.getProperty("/todos_tramos");

			if (bNeedLoadWBS) {
				this.oDataModel.s4h.read("/ZXC_WBSELEMENTWITHVERSION_CDS/ZXC_WBSELEMENTWITHVERSION/?" + filtro, null, null, false,
					function (oData) {
						model = oData.results;
						for (var i = 0; i < oData.results.length; i++) {
							datos.push(oData.results[i]);
						}
						console.log(model);
					},
					function (oResponse) {
						console.log(oResponse);
					}
				);
				oModel.setProperty("/combosData/pep", model);
				oModel.setProperty("/todos_tramos", datos);
				//apagar el flag para no buscar de nuevo elementos PEP
				if (model.length > 0) {
					oModel.setProperty("/bNeedLoadWBS", false)
				}
			}
		},
		//Consulta para traer los tramos (elemento pep).
		consultarPep1: function (Project) {
			var oModel = this.getView().getModel();
			this.setearAbscisas();
			/*if (this._getCore().byId("proyect")) {
				var proyecto = this._getCore().byId("proyect").getSelectedKey();
			} else {
				var proyecto = oModel.getProperty("/activities/0/PROYECTO");
			}
			if (proyecto === "") {
				var proyecto = oModel.getProperty("/activities/0/PROYECTO");
			}*/
			if (Project) {
				var proyecto = Project.PROYECTO;
			}
			//debugger;
			/* 
			 **SE DESHABILITA FUNCIONALIDAD PARA MEJORAR RENDIMIENTO
			 */
			var sField = "&$select=WBSDescription,WBSElement"
			var filtro = "$filter= Version eq '' and Project eq '" + proyecto + "'";
			filtro += sField;
			var model = [];
			var datos = oModel.getProperty("/todos_tramos");
			this.oDataModel.s4h.read("/ZXC_WBSELEMENTWITHVERSION_CDS/ZXC_WBSELEMENTWITHVERSION/?" + filtro, null, null, false,
				function (oData) {
					model = oData.results;
					for (var i = 0; i < oData.results.length; i++) {
						datos.push(oData.results[i]);
					}
					console.log(model);
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			oModel.setProperty("/combosData/pep", model);
			oModel.setProperty("/todos_tramos", datos);

		},
		//Consulta para setear todos los tramos (elemento pep).
		consultarallpep: function () {
			var oModel = this.getView().getModel();
			this.consultarPep();
			this.setearAbscisas();
			this._openPep();
			if (this._getCore().byId("proyect")) {
				var proyecto = this._getCore().byId("proyect").getSelectedKey();
			} else {
				var proyecto = oModel.getProperty("/activities/0/PROYECTO");
			}
			var filtro = "$filter= Version eq '' and Project eq '" + proyecto + "'";
			var model = [];
			this.oDataModel.s4h.read("/ZXC_WBSELEMENTWITHVERSION_CDS/ZXC_WBSELEMENTWITHVERSION/?" + filtro, null, null, false,
				function (oData) {
					console.log(oData.results);
					for (var i = 0; i < oData.results.length; i++) {
						var proyecto = oData.results[0].Project + " " + oData.results[0].WBSDescription
						var codigo = oData.results[i].WBSElement;
						var descripcion = oData.results[i].WBSDescription;
						var arreglo = {
							proyecto: proyecto,
							name: codigo,
							descripcion: descripcion
						};
						model.push(arreglo);
					}
					console.log(model);
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			oModel.setProperty("/jerarquiapep", model);
		},
		//Consulta para traer las actividades del elemento pep del proyecto.
		consultarActividad: function (tramo) {
			var oModel = this.getView().getModel();
			var that = this;
			//Filtro desde la ventana emergente del Elemento Pep.
			if (tramo) {
				if (tramo.mParameters) {
					tramo = tramo.mParameters.selectedItem.mProperties.key;
				}
				if (this._getCore().byId("proyect")) {
					var proyecto = this._getCore().byId("proyect").getSelectedKey();
					var tramo = tramo;
				} else {
					var proyecto = oModel.getProperty("/activities/0/PROYECTO");
					var tramo = tramo;
				}
				var filtro = "$filter= Version eq '' and Project eq '" + proyecto + "'and WBSElement eq '" + tramo + "'";
				var model = [];
				this.oDataModel.s4h.read("/ZXC_WBSELEMENTWITHVERSION_CDS/ZXC_WBSELEMENTWITHVERSION/?" + filtro, null, null, false,
					function (oData) {
						if (oData.results.length > 0) {
							that._getCore().byId("tramo").setSelectedKey(oData.results[0].WBSElement);
						}
					},
					function (oResponse) {
						console.log(oResponse);
					}
				);
				//filtro de consulta actividad.
				var filtro = "$filter= Version eq '' and Project eq '" + proyecto +
					"' and WBSElement eq '" + tramo + "'";
				//Filro cuando carga la aplicación.
			} else {
				if (this._getCore().byId("proyect")) {
					var proyecto = this._getCore().byId("proyect").getSelectedKey();
					var tramo = this._getCore().byId("tramo").getSelectedKey();
				} else {
					var proyecto = oModel.getProperty("/activities/0/PROYECTO");
					var tramo = oModel.getProperty("/activities/0/TRAMO");
				}
				var filtro = "$filter= Version eq '' and Project eq '" + proyecto +
					"' and WBSElement eq '" + tramo + "'";
			}
			var model = [];
			var data = oModel.getProperty("/todas_actividades");
			this.oDataModel.s4h.read("/ZXC_NETWORKACTIVITYWITHVRS_CDS/ZXC_NETWORKACTIVITYWITHVRS?" + filtro, null, null, false,
				function (oData) {
					for (var i = 0; i < oData.results.length; i++) {
						if (oData.results[i].Item !== "") {
							if (oData.results[i].NetworkActivityElement === "") {
								model.push(oData.results[i]);
								data.push(oData.results[i]);
							}
							oModel.setProperty("/GRAFO_ACT", oData.results[i].ProjectNetwork);
							var dato = oData.results[i].NetworkActivity;
							oModel.setProperty("/OPERACION_ACT", dato);
							oModel.setProperty("/clave_modelo", oData.results[i].StandardTextInternalID);
						}
					}
					console.log(model);
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			oModel.setProperty("/combosData/_actividad", model);
			oModel.setProperty("/todas_actividades", data);
			//oModel.setProperty("/combosData/componente", model);
			this.getView().getModel().setProperty("/combosData/detalle_actividad", []);
			oModel.setProperty("/datoserp", model);
			var data = [];
			var proyecto = this._getCore().byId("proyect").getSelectedKey();
			var tramo = this._getCore().byId("tramo").getSelectedKey();
			var actividad = this._getCore().byId("actividad").getSelectedKey();
			var filtro = "$filter= PROYECTO eq '" + proyecto + "' and ELEMENTO_PEP eq '" + tramo + "' and GRAFO_OPERACION eq '" + actividad +
				"' ";
			this.oDataModel.hcp.read("/T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS?" + filtro, null, null, false,
				function (oData) {
					data = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			this.getView().getModel().setProperty("/combosData/detalle_actividad", data);
		},
		//Consulta para traer las actividades del elemento pep del proyecto.
		consultaractividad_: function (PROYECTO) {
			var oModel = this.getView().getModel();
			var that = this;
			/*	if (this._getCore().byId("proyect")) {
					var proyecto = this._getCore().byId("proyect").getSelectedKey();
					var tramo = this._getCore().byId("tramo").getSelectedKey();
				} else {*/
			/*	var proyecto = oModel.getProperty("/activities/0/PROYECTO");
				var tramo = oModel.getProperty("/activities/0/TRAMO");
			*/ //}
			var sFields = "&$select=NetworkActivityDescription,NetworkActivityConcatenatedID";
			var filtro = "$filter= Version eq '' and Project eq '" + PROYECTO.PROYECTO + "' and WBSElement eq '" + PROYECTO.TRAMO + "'";
			filtro += sFields;
			var model = [];
			var datos = oModel.getProperty("/todas_actividades");
			this.oDataModel.s4h.read("/ZXC_NETWORKACTIVITYWITHVRS_CDS/ZXC_NETWORKACTIVITYWITHVRS?" + filtro, null, null, false,
				function (oData) {
					for (var i = 0; i < oData.results.length; i++) {
						if (oData.results[i].Item !== "") {
							if (oData.results[i].NetworkActivityElement === "") {
								model.push(oData.results[i]);
								datos.push(oData.results[i]);
							}
							oModel.setProperty("/GRAFO_ACT", oData.results[i].ProjectNetwork);
							var dato = oData.results[i].NetworkActivity;
							oModel.setProperty("/OPERACION_ACT", dato);
							//oModel.setProperty("/clave_modelo", oData.results[i].StandardTextInternalID);
						}
					}
					console.log(model);
					//console.log(submodelo);
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			//oModel.setProperty("/combosData/_actividad", model);
			oModel.setProperty("/todas_actividades", datos);
			this.getView().getModel().setProperty("/combosData/detalle_actividad", []);
			oModel.setProperty("/datoserp", datos);
			var data = [];
			//var filtro = "$filter= CLAVE_MODELO eq '" + oModel.getProperty("/clave_modelo") + "'";
			/* SE COMENTA ESTA SECCIÓN PARA VALIDAR RENDIMIENTO
			this.oDataModel.hcp.read("/T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS", null, null, false,
				function (oData) {
					data = oData.results;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			this.getView().getModel().setProperty("/todas_subactividades", data);
			*/
		},
		//Consulta para traer los componentes de la actividad.
		consultarComponente: function () {
			var oModel = this.getView().getModel();
			//Evaluar la clasificacion de la Actividad.
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			var filter = "$filter= ID_EQUIPO eq '" + id_equipo + "'";
			var clasificacion;
			this.oDataModel.hcp.read("/T_EQUIPO?" + filter, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						clasificacion = oData.results[0].CLASIFICACION;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			if (this._getCore().byId("proyect")) {
				var proyecto = this._getCore().byId("proyect").getSelectedKey();
				var grafo = oModel.getProperty("/GRAFO_ACT");
				var operacion = oModel.getProperty("/OPERACION_ACT");
			} else {
				var proyecto = oModel.getProperty("/activities/0/PROYECTO");
				var grafo = oModel.getProperty("/GRAFO_ACT");
				var operacion = oModel.getProperty("/OPERACION_ACT");
			}
			var sFields = "&$select=SuperiorNtwkActivityExternalID,NetworkActivityDescription";
			var filtro = "$filter= Version eq '' and Project eq '" + proyecto + "'and ProjectNetwork eq '" + grafo +
				"' and NetworkActivity eq '" + operacion + "'";
			filtro += sFields;
			var model = [];
			var componentes = oModel.getProperty("/todos_componentes");
			this.oDataModel.s4h.read("/ZXC_NETWORKACTIVITYWITHVRS_CDS/ZXC_NETWORKACTIVITYWITHVRS?" + filtro, null, null,
				false,
				function (oData) {
					for (var i = 0; i < oData.results.length; i++) {
						if (oData.results[i].NetworkActivityElement !== "") {
							if (oData.results[i].WorkCenter === clasificacion) {
								model.push(oData.results[i]);
								componentes.push(oData.results[i]);
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			console.log(model);
			oModel.setProperty("/combosData/componente", model);
			oModel.setProperty("/todos_componentes", componentes);
		},
		//Consulta para traer los componentes de la actividad.
		consultarComponente1: function (proyecto) {
			var oModel = this.getView().getModel();
			//Evaluar la clasificacion de la Actividad.
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			var filter = "$filter= ID_EQUIPO eq '" + id_equipo + "'";
			var clasificacion;
			this.oDataModel.hcp.read("/T_EQUIPO?" + filter, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						clasificacion = oData.results[0].CLASIFICACION;
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			var grafo = oModel.getProperty("/GRAFO_ACT");
			var operacion = oModel.getProperty("/OPERACION_ACT");
			var filtro = "$filter= Version eq '' and Project eq '" + proyecto + "' and ProjectNetwork eq '" + grafo + "'";
			var model = [];
			var componentes = oModel.getProperty("/todos_componentes");
			if (grafo) {
				this.oDataModel.s4h.read("/ZXC_NETWORKACTIVITYWITHVRS_CDS/ZXC_NETWORKACTIVITYWITHVRS?" + filtro, null, null,
					false,
					function (oData) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].NetworkActivityElement !== "") {
								if (oData.results[i].WorkCenter === clasificacion) {
									//model.push(oData.results[i]);
									componentes.push(oData.results[i]);
								}
							}
						}
					},
					function (oResponse) {
						console.log(oResponse);
					}
				);
			}
			console.log(componentes);
			//oModel.setProperty("/combosData/componente", model);
			oModel.setProperty("/todos_componentes", componentes);
		},
		//Establecer los datos de la actividad que se le envían al ERP.
		setearActividad1: function (id_actividad, proyecto) {
			var oModel = this.getView().getModel();
			var oData = oModel.getProperty("/datoserp");
			for (var i = 0; i < oData.length; i++) {
				if (oData[i].NetworkActivityConcatenatedID === id_actividad) {
					oModel.setProperty("/GRAFO_ACT", oData[i].ProjectNetwork);
					var dato = oData[i].NetworkActivity;
					//var nombre = dato.substring(dato.length - 1);
					oModel.setProperty("/OPERACION_ACT", dato);
					var dato1 = oData[i].SuperiorNtwkActivityExternalID;
					//var nombre1 = dato1.substring(dato1.length - 1);
					oModel.setProperty("/COD_ACT", dato1);
				}
			}
			this.consultarComponente1(proyecto);
		},
		//Establecer los datos de la actividad que se le envían al ERP.
		setearactividad: function (oEvent) {
			var oModel = this.getView().getModel();
			var oData = oModel.getProperty("/datoserp");
			var proyecto = this._getCore().byId("proyect").getSelectedKey();
			var tramo = this._getCore().byId("tramo").getSelectedKey();
			var actividad = this._getCore().byId("actividad").getSelectedKey();
			//if (oEvent) {
			/*for (var i = 0; i < oData.length; i++) {
				if (oEvent.getParameters().selectedItem.getText() === oData[i].NetworkActivityDescription) {
					oModel.setProperty("/clave_modelo", oData[i].StandardTextInternalID);
				}
			}*/
			this.getView().getModel().setProperty("/combosData/detalle_actividad", []);
			var data = [];
			var datosgenerales = this.getView().getModel().getProperty("/todas_subactividades");
			if (actividad === "5000523  / 5129") {
				var filtro = "$filter= PROYECTO eq '" + proyecto + "' and ELEMENTO_PEP eq '" + tramo + "' and GRAFO_OPERACION eq '" + actividad +
					"' ";
				this.oDataModel.hcp.read("/T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS?" + filtro, null, null, false,
					function (oData) {
						data = oData.results;
						for (var i = 0; i < oData.results.length > 0; i++) {
							datosgenerales = datosgenerales.concat(oData.results[i]);
						}
					},
					function (oResponse) {
						console.log(oResponse);
					});
				this.getView().getModel().setProperty("/combosData/detalle_actividad", data);
				this.getView().getModel().setProperty("/todas_subactividades", datosgenerales);

			} else if (actividad === "5000523  / 5131") {
				var filtro = "$filter= PROYECTO eq '" + proyecto + "' and ELEMENTO_PEP eq '" + tramo + "' and GRAFO_OPERACION eq '" + actividad +
					"' ";
				this.oDataModel.hcp.read("/T_MQ_SUBACTIVIDADES_HORAS_APAGADO?" + filtro, null, null, false,
					function (oData) {
						data = oData.results;
						for (var i = 0; i < oData.results.length > 0; i++) {
							datosgenerales = datosgenerales.concat(oData.results[i]);
						}
					},
					function (oResponse) {
						console.log(oResponse);
					});
				this.getView().getModel().setProperty("/combosData/detalle_actividad", data);
				this.getView().getModel().setProperty("/todas_subactividades", datosgenerales);
			}
			//}
			var campo = oModel.getProperty("/activities");
			var id_actividad = oModel.getProperty("/actividad/ACTIVIDAD");
			if (!id_actividad) {
				for (var i = 0; i < campo.length; i++) {
					if (campo[i].ACTIVIDAD !== "") {
						id_actividad = campo[i].ACTIVIDAD;
					}
				}
			}
			for (var i = 0; i < oData.length; i++) {
				if (oData[i].NetworkActivityConcatenatedID === id_actividad) {
					oModel.setProperty("/GRAFO_ACT", oData[i].ProjectNetwork);
					var dato = oData[i].NetworkActivity;
					//var nombre = dato.substring(dato.length - 1);
					oModel.setProperty("/OPERACION_ACT", dato);
					/*	var dato1 = oData[i].SuperiorNtwkActivityExternalID;
						//var nombre1 = dato1.substring(dato1.length - 1);
						oModel.setProperty("/COD_ACT", dato1);*/
				}
			}
			this.consultarComponente();
		},
		//Establecer el tramo en la pestaña de actividades al seleccionar la ubicación.
		seteartramo: function () {
			var oModel = this.getView().getModel();
			var project = this._getCore().byId("abscisa").getSelectedKey();;
			var elementopep;
			var idProject = oModel.getProperty("/actividad/PROYECTO");
			var filtro1 = "$filter= UBICACION_INICIAL eq '" + project + "' and PROYECTO eq '" + idProject + "'";
			this.oDataModel.hcp.read("T_UBICACIONES?" + filtro1, null, null, false,
				function (oData) {
					elementopep = oData.results[0].ELEMENTO_PEP;
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/actividad/TRAMO", elementopep);
			this.consultarActividad(elementopep);
		},
		//Establecer el codigo de componente que se envia al ERP.
		setearComponente: function () {
			var oModel = this.getView().getModel();
			var componente = this._getCore().byId("componente").getSelectedKey();
			oModel.setProperty("/COD_ACT", componente);
		},
		//Función para establecer cantidad de horas x actividad.
		restaHoras: function (hora_inicio, hora_fin) {
			var hora1 = hora_inicio.split(":"),
				hora2 = hora_fin.split(":"),
				t1 = new Date(),
				t2 = new Date();

			t1.setHours(hora1[0], hora1[1], hora1[2]);
			t2.setHours(hora2[0], hora2[1], hora2[2]);

			//Aquí hago la resta
			//t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes(), t1.getSeconds() - t2.getSeconds());
			var hora = t1.getHours() - t2.getHours();
			var minutos = t1.getMinutes() - t2.getMinutes();
			minutos = Math.abs(parseInt(minutos * 1.6));
			hora = Math.abs(parseInt(hora));
			var resta = hora + "." + minutos;
			console.log(resta);
			return resta;
		},
		//Formateo de hora para envío al ERP.
		formatoHoras: function (hora) {
			var hora1 = hora.split(":");
			var time = "PT" + hora1[0] + "H" + hora1[1] + "M" + hora1[2] + "S";
			console.log(time);
			return time;
		},
		//Validación para que el horometro este dentro del rango establecido.
		validarHorometro: function () {
			var oModel = this.getView().getModel();
			var horometro1 = oModel.getProperty("/actividad/HOROMETRO_INICIAL");
			var horometro2 = oModel.getProperty("/informacion_general/HOROMETRO_INICIAL");
			if (horometro1 < horometro2) {
				sap.m.MessageBox.error("El valor del Horómetro Inicial no puede ser inferior al diligenciado en Información general.");
				oModel.setProperty("/actividad/HOROMETRO_INICIAL", 0);
			}
		},
		//Función para promediar los horómetros de información general.
		setearHorometro: function () {
			var oModel = this.getView().getModel();
			var fechamq = oModel.getProperty("/fecha_mq");
			var id_equipo = oModel.getProperty("/ID_EQUIPO");
			var horometro_inicial;
			var horometro_final;
			var odometro_inicial;
			var odometro_final;
			var promedio;
			var promedio_odometro;
			this.oDataModel.hcp.read("/T_PARAMETROS_APP", null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].DESCRIPCION_PARAMETRO === "PROMEDIO_HOROMETRO") {
								promedio = oData.results[i].VALOR;
							}
							if (oData.results[i].DESCRIPCION_PARAMETRO === "PROMEDIO_ODOMETRO") {
								promedio_odometro = oData.results[i].VALOR;
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			var filtro = "$filter= ID_EQUIPO eq '" + id_equipo + "'";
			this.oDataModel.hcp.read("T_MQ_INFORMACION?" + filtro, null, null, false,
				function (oData) {
					if (oData.results.length > 0) {
						horometro_inicial = oData.results[oData.results.length - 1].HOROMETRO_FINAL;
						odometro_inicial = oData.results[oData.results.length - 1].KILOMETRO_FINAL;
						horometro_final = 0; //parseFloat(horometro_inicial); //+ parseFloat(promedio);
						odometro_final = 0; //parseFloat(odometro_inicial); //+ parseFloat(promedio_odometro);
					}
				},
				function (oResponse) {
					console.log(oResponse);
				});
			oModel.setProperty("/informacion_general/HOROMETRO_INICIAL", horometro_inicial);
			oModel.setProperty("/informacion_general/HOROMETRO_FINAL", horometro_final);
			oModel.setProperty("/informacion_general/KILOMETRO_INICIAL", odometro_inicial);
			oModel.setProperty("/informacion_general/KILOMETRO_FINAL", odometro_final);
		},
		//Función para validar que el horómetro no supere las 24 horas.
		validarRangoHorometro: function () {
			var oModel = this.getView().getModel();
			var horometro1 = oModel.getProperty("/informacion_general/HOROMETRO_INICIAL");
			var horometro2 = oModel.getProperty("/informacion_general/HOROMETRO_FINAL");
			var diferencia = horometro2 - horometro1;
			if (diferencia > 24) {
				sap.m.MessageBox.error("La diferencia entre Horometro inicial y final no pueden superar las 24 horas.");
				oModel.setProperty("/informacion_general/HOROMETRO_FINAL", 0);
			}
		},
		//función para consultar actividad.
		selectionelementopep: function (oEvent) {
			var context = oEvent.getParameters().rowBindingContext.getObject();
			if (context.name) {
				var valor = context.name;
				this.consultarActividad(valor);
			}
			console.log(context);
			this._closePep();
		},
		//Funciones de actividad.
		//Ventana emergente para las actividades.
		addActivity: function () {
			this._openActivity();
		},
		//Abrir ventana emergente de actividades.	
		_openActivity: function () {
			//this.consultarCds();
			if (!this._oActivity) {
				// create dialog via fragment factory
				this._oActivity = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.activity", this);
				this._getView().addDependent(this._oActivity);
			}
			this._oActivity.open();
		},
		//Cerrar ventana emergente de actividades.
		_closeActivity: function () {
			this._oActivity.close();
		},
		//Guardar actividad en SCP.
		_saveActivity: function () {
			var oModel = this.getView().getModel();
			var oData = oModel.getProperty("/actividad");
			var datosActuales = oModel.getProperty("/activities");
			var modelo = [];
			var obj = {
				ID_ACTIVIDAD: 1,
				ID_EQUIPO: this.ID_EQUIPO,
				TIPO_IMPUTACION: oData.TIPO_IMPUTACION,
				OBJETO_IMPUTACION: oData.OBJETO_IMPUTACION,
				HORA_INICIO: oData.HORA_INICIO,
				HORA_FIN: oData.HORA_FIN,
				HOROMETRO_INICIAL: oData.HOROMETRO_INICIAL,
				HOROMETRO_FINAL: oData.HOROMETRO_FINAL,
				TIPO_ACTIVIDAD: oData.TIPO_ACTIVIDAD,
				ACTIVIDAD: oData.ACTIVIDAD,
				TIPO_UBICACION: oData.TIPO_UBICACION,
				INICIO_ABSCISA: oData.INICIO_ABSCISA,
				FIN_ABSCISA: oData.FIN_ABSCISA,
				VALE_INTERNO: oData.VALE_INTERNO,
				VALE_PROVEEDOR: oData.VALE_PROVEEDOR
			};
			modelo.push(obj);
			for (var i = 0; i < modelo.length; i++) {
				modelo[i].HOROMETRO_INICIAL = parseFloat(modelo[i].HOROMETRO_INICIAL);
				modelo[i].HOROMETRO_FINAL = parseFloat(modelo[i].HOROMETRO_FINAL);
				modelo = JSON.stringify(modelo);
				//Guardar Actividad.
				jQuery.ajax({
					url: "/HANA_DB/logical/xs_service/sincronizar_actividad.xsjs",
					method: "POST",
					async: false,
					data: modelo,
					success: function (oResponse) {
						console.log(oResponse);
						datosActuales.push(oResponse[0]);
					},
					error: function (oError) {
						console.log(oError);
					}
				});
				//Convertir a Json el array.
				modelo = JSON.parse(modelo);
			}
			oModel.setProperty("/activities", datosActuales);
			oModel.setProperty("/actividad", []);
			this._oActivity.close();
		},
		//Agregar registro en seccion de Actividades.
		agregarActividad: function () {
			var oModel = this.getView().getModel();
			var that = this;
			var tActividades = oModel.getProperty("/activities");
			var oData = oModel.getProperty("/actividad");
			for (var i = 0; i < tActividades.length; i++) {
				if (tActividades[i].HORA_INICIO === oData.HORA_INICIO && tActividades[i].HORA_FIN === oData.HORA_FIN) {
					sap.m.MessageToast.show("No pueden tener las mismas horas dos actividades.", {
						duration: 20000
					});
					return;
				}
			}
			if (oData.TIPO_IMPUTACION === "M") {
				var horainicio = this.formatoHoras(oData.HORA_INICIO);
				var horafin = this.formatoHoras(oData.HORA_FIN);
				oModel.setProperty("/AUFNR_PLANTA", oData.ORDEN_PLANTA);
				oModel.setProperty("/hora_inicio", horainicio);
				oModel.setProperty("/hora_fin", horafin);
				var obj = {
					ID_ACTIVITY: 1,
					ID_EQUIPO: this.ID_EQUIPO,
					TIPO_IMPUTACION: oData.TIPO_IMPUTACION,
					PLANTA: oData.PLANTA,
					ORDEN: oData.ORDEN_PLANTA,
					PROYECTO: "",
					TRAMO: "",
					ACTIVIDAD: "",
					SUBACTIVIDAD: "",
					HORA_INICIO: oData.HORA_INICIO,
					HORA_FIN: oData.HORA_FIN,
					HOROMETRO_INICIAL: "",
					HOROMETRO_FINAL: "",
					TIPO_UBICACION: oData.TIPO_UBICACION,
					INICIO_ABSCISA: oData.INICIO_ABSCISA,
					FIN_ABSCISA: oData.FIN_ABSCISA,
					VALE_INTERNO: oData.VALE_INTERNO,
					VALE_PROVEEDOR: oData.VALE_PROVEEDOR,
					MQ_CONSECUTIVO: oModel.getProperty("/MQ_CONSECUTIVO")
				};
				this.getView().byId("plantas").setVisible(true);
				this.getView().byId("orden").setVisible(true);
				this.getView().byId("proyect").setVisible(false);
				this.getView().byId("tramo").setVisible(false);
				this.getView().byId("actividad").setVisible(false);
				//this.getView().byId("item").setVisible(false);
			} else if (oData.TIPO_IMPUTACION === "A") {
				/*if (!oData.VALE_INTERNO || oData.VALE_INTERNO === "0") {
					sap.m.MessageToast.show("El número de vale es obligatorio.", {
						duration: 20000
					});
					return;
				}*/
				var obj = {
					ID_ACTIVITY: 1,
					ID_EQUIPO: this.ID_EQUIPO,
					TIPO_IMPUTACION: oData.TIPO_IMPUTACION,
					PLANTA: "",
					ORDEN: "",
					PROYECTO: oData.PROYECTO,
					TRAMO: oData.TRAMO,
					ACTIVIDAD: oData.ACTIVIDAD,
					SUBACTIVIDAD: oData.SUBACTIVIDAD,
					HORA_INICIO: oData.HORA_INICIO,
					HORA_FIN: oData.HORA_FIN,
					HOROMETRO_INICIAL: oData.HOROMETRO_INICIAL,
					HOROMETRO_FINAL: oData.HOROMETRO_FINAL,
					TIPO_UBICACION: oData.TIPO_UBICACION,
					INICIO_ABSCISA: oData.INICIO_ABSCISA,
					FIN_ABSCISA: oData.FIN_ABSCISA,
					VALE_INTERNO: oData.VALE_INTERNO,
					VALE_PROVEEDOR: oData.VALE_PROVEEDOR,
					MQ_CONSECUTIVO: oModel.getProperty("/MQ_CONSECUTIVO"),
					COMPONENTE: oData.COMPONENTE
				};
				if (oData.HORA_INICIO && oData.HORA_FIN) {
					var horas_trabajadas = Math.abs(this.restaHoras(oData.HORA_INICIO, oData.HORA_FIN));
					oModel.setProperty("/horas_trabajadas", horas_trabajadas);
				}
				that.getView().byId("proyect").setVisible(true);
				that.getView().byId("tramo").setVisible(true);
				that.getView().byId("actividad").setVisible(true);
				//this.getView().byId("item").setVisible(true);
				this.getView().byId("plantas").setVisible(false);
				this.getView().byId("orden").setVisible(false);
				this.consultarActividad();
			}
			tActividades.push(obj);
			oModel.setProperty("/activities", tActividades);
			this._oActivity.close();
		},
		//Función para guardar las actividades.
		guardarActividad: function () {
			var oModel = this.getView().getModel();
			var oActividad = oModel.getProperty("/activities");
			var flag = false;
			for (var i = 0; i < oActividad.length; i++) {
				var model = [];
				oActividad[i].HOROMETRO_INICIAL = parseFloat(oActividad[i].HOROMETRO_INICIAL);
				oActividad[i].HOROMETRO_FINAL = parseFloat(oActividad[i].HOROMETRO_FINAL);
				if (!oActividad[i].TRAMO) {
					oActividad[i].TRAMO = "";
				}
				if (!oActividad[i].ACTIVIDAD) {
					oActividad[i].ACTIVIDAD = "";
				}
				if (!oActividad[i].ORDEN) {
					oActividad[i].ORDEN = "";
				}
				if (!oActividad[i].SUBACTIVIDAD) {
					oActividad[i].SUBACTIVIDAD = "";
				}
				if (!oActividad[i].COMPONENTE) {
					oActividad[i].COMPONENTE = "";
				}
				if (!oActividad[i].FIN_ABSCISA) {
					oActividad[i].FIN_ABSCISA = "";
				}
				if (!oActividad[i].VALE_PROVEEDOR) {
					oActividad[i].VALE_PROVEEDOR = "";
				}
				var obj = {
					ID_ACTIVITY: oActividad[i].ID_ACTIVITY,
					ID_EQUIPO: this.ID_EQUIPO,
					TIPO_IMPUTACION: oActividad[i].TIPO_IMPUTACION,
					PLANTA: oActividad[i].PLANTA,
					ORDEN: oActividad[i].ORDEN,
					PROYECTO: oActividad[i].PROYECTO,
					TRAMO: oActividad[i].TRAMO,
					ACTIVIDAD: oActividad[i].ACTIVIDAD,
					SUBACTIVIDAD: oActividad[i].SUBACTIVIDAD,
					COMPONENTE: oActividad[i].COMPONENTE,
					HORA_INICIO: oActividad[i].HORA_INICIO,
					HORA_FIN: oActividad[i].HORA_FIN,
					HOROMETRO_INICIAL: 0,
					HOROMETRO_FINAL: 0,
					TIPO_UBICACION: "",
					INICIO_ABSCISA: oActividad[i].INICIO_ABSCISA,
					FIN_ABSCISA: oActividad[i].FIN_ABSCISA,
					VALE_INTERNO: oActividad[i].VALE_INTERNO,
					VALE_PROVEEDOR: oActividad[i].VALE_PROVEEDOR,
					MQ_CONSECUTIVO: oModel.getProperty("/MQ_CONSECUTIVO")
				};
				model.push(obj);
				model = JSON.stringify(model);
				console.log(model);
				//Guardar Actividad.
				jQuery.ajax({
					url: "/HANA_DB/logical/xs_service/sincronizar_actividad1.xsjs",
					method: "POST",
					async: false,
					data: model,
					success: function (oResponse) {
						console.log(" Actividad guardada correctamente " + oResponse);
						flag = true;
					},
					error: function (oError) {
						console.log(oError);
					}
				});
				//Convertir a Json el array.
				model = JSON.parse(model);
			}
			if (flag) {
				//sap.m.MessageBox.success("Actividad guardada correctamente");
			}

		},
		//Agregar Peaje.
		addToll: function () {
			this._openToll();
		},
		//Abrir ventana emergente de Peaje.
		_openToll: function () {
			// validate dialog lazily
			if (!this._oToll) {
				// create dialog via fragment factory
				this._oToll = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.toll", this);
				this._getView().addDependent(this._oToll);
			}
			this._oToll.open();
		},
		//Cerrar ventana emergente de Peaje.
		_closeToll: function () {
			this._oToll.close();
		},
		//Mostrar peajes Creados.
		_openPeaje: function (oEvent) {
			// validate dialog lazily
			this.obtenerIdActividad(oEvent);
			this.leerPeajes();
			if (!this._oPeaje) {
				// create dialog via fragment factory
				this._oPeaje = sap.ui.xmlfragment("mqa.ui5.mq.view.fragment.peajes", this);
				this._getView().addDependent(this._oPeaje);
			}
			this._oPeaje.open();
		},
		//Cerrar ventana de peajes Creados.
		_closePeaje: function () {
			this._oPeaje.close();
		},
		//Función para habilitar o deshabilitar campos en actividades(orden o actividad).
		evaluarImputacion: function () {
			var oModel = this.getView().getModel();
			var imputacion = this._getCore().byId("imputacion").getValue();
			if (imputacion === "ORDEN PRODUCCION") {
				this._getCore().byId("plantas").setVisible(true);
				this._getCore().byId("observation").setVisible(true);
				this._getCore().byId("txtP").setVisible(true);
				this._getCore().byId("objeto_imp").setVisible(true);
				//
				this._getCore().byId("proyect").setVisible(false);
				this._getCore().byId("tramo").setVisible(false);
				this._getCore().byId("actividad").setVisible(false);
				this._getCore().byId("componente").setVisible(false);
				//this._getCore().byId("item").setVisible(false);
				this._getCore().byId("textPY").setVisible(false);
				this._getCore().byId("textTR").setVisible(false);
				this._getCore().byId("textA").setVisible(false);
				this._getCore().byId("textC").setVisible(false);
				//this._getCore().byId("textI").setVisible(false);
			} else {
				/*	var equipo = this.getView().getModel().getProperty("/ID_EQUIPO");
					var filtro = "$filter= ID_EQUIPO eq '" + equipo + "'";
					var proyecto;
					this.oDataModel.hcp.read("/ASIGNACION_EQUIPO?" + filtro, null, null, false,
						function (oData) {
							if (oData.results.length > 0) {
								proyecto = oData.results[0].PROYECTO;
							}
						},
						function (oResponse) {
							console.log(oResponse);
						});*/
				this.consultarPep();
				this._getCore().byId("proyect").setVisible(true);
				this._getCore().byId("tramo").setVisible(true);
				this._getCore().byId("actividad").setVisible(true);
				this._getCore().byId("componente").setVisible(true);
				//this._getCore().byId("item").setVisible(true);
				this._getCore().byId("textPY").setVisible(true);
				this._getCore().byId("textTR").setVisible(true);
				this._getCore().byId("textA").setVisible(true);
				this._getCore().byId("textC").setVisible(true);
				//this._getCore().byId("textI").setVisible(true);
				//
				this._getCore().byId("plantas").setVisible(false);
				this._getCore().byId("observation").setVisible(false);
				this._getCore().byId("txtP").setVisible(false);
				this._getCore().byId("objeto_imp").setVisible(false);
				//var filtro = "$filter= ID_PERSONA eq 1";
				var proyecto = oModel.getProperty("/CENTRO");
				//oModel.setProperty("/actividad/PROYECTO", proyecto);
				oModel.refresh();
				this._getCore().byId("proyect").setSelectedKey(proyecto);
				this.consultarPep();
			}
		},
		//Consulta de plantas del ERP.
		definirPlanta: function (oEvent) {
			var oModel = this.getView().getModel();
			var model = [];
			var ordenes = [];
			var oItem = oEvent.getSource(),
				cantidad = oItem.mProperties.selectedKey;
			this.oDataModel.erp.read("/ConsultaPlantasSet", null, null, false,
				function (oData) {
					oData.results.sort();
					for (var i = 0; i < oData.results.length; i++) {
						if (cantidad === oData.results[i].Werks) {
							if (model.length > 0) {
								if (model[0].Aufnr !== oData.results[i].Aufnr) {

								}
							} else {
								ordenes.push(oData.results[i]);
							}
						}
					}
				},
				function (oResponse) {
					console.log(oResponse);
				}
			);
			oModel.setProperty("/combosData/orden_planta", ordenes);
			/*	var categoria = oModel.getProperty("/actividad/PLANTA");
				oModel.setProperty("/actividad/ORDEN_PLANTA", categoria);*/
		},
		//Consulta de subactividades.
		consultaSubactividades: function (proyecto, tramo, actividad) {
			var data = this.getView().getModel().getProperty("/todas_subactividades");
			if (actividad === "5000523  / 5129") {
				var filtro = "$filter= PROYECTO eq '" + proyecto + "' and ELEMENTO_PEP eq '" + tramo + "' and GRAFO_OPERACION eq '" + actividad +
					"' ";
				this.oDataModel.hcp.read("/T_MQ_SUBACTIVIDADES_ADMINISTRATIVAS?" + filtro, null, null, false,
					function (oData) {
						for (var i = 0; i < oData.results.length > 0; i++) {
							data = data.concat(oData.results[i]);
						}
					},
					function (oResponse) {
						console.log(oResponse);
					});

			} else if (actividad === "5000523  / 5131") {
				var filtro = "$filter= PROYECTO eq '" + proyecto + "' and ELEMENTO_PEP eq '" + tramo + "' and GRAFO_OPERACION eq '" + actividad +
					"' ";
				this.oDataModel.hcp.read("/T_MQ_SUBACTIVIDADES_HORAS_APAGADO?" + filtro, null, null, false,
					function (oData) {
						for (var i = 0; i < oData.results.length > 0; i++) {
							data = data.concat(oData.results[i]);
						}
					},
					function (oResponse) {
						console.log(oResponse);
					});
				this.getView().getModel().setProperty("/todas_subactividades", data);
			}
		},
		//Validar cantidad maxima de materiales de Consumo.
		cantidadMaxima: function (oEvent) {
			var oModel = this.getView().getModel();
			var cantidadMax = oModel.getProperty("/cantidad_max");
			var oItem = oEvent.getSource(),
				bandera = parseFloat(oItem.getSelectedKey());
			if (bandera === 1) {
				oItem.getParent().getCells()[1].setValue(cantidadMax);
			} else {
				oItem.getParent().getCells()[1].setValue(0);
			}
		},
		/* _getCore
		 * @returns {sap.ui.core} the core instance
		 * Convenience method for getting the core controller of the application.
		 */
		_getCore: function () {
			return sap.ui.getCore();
		},

		/* _getModel
		 * @returns {sap.ui.model.Model} the model instance
		 * Convenience method for getting the view model by name in every controller of the application.
		 */
		_getModel: function () {
			return this.getView().getModel();
		},

		/* _getView
		 * @returns {sap.ui.model.View} the View instance
		 * Convenience method for getting the view model by name in every controller of the application.
		 */
		_getView: function () {
			return this.getView();
		},
		/**
		 * Realiza una petición GET en ERP(CDS, servicios) y en SCP (tablas de HANA XS).
		 * @param {string} sTableName - Nombre de la URI (ERP) o tabla (SCP).
		 * @param {string} sOdataParams - queryOptions para realizar la consulta a la URI
		 * @param {string} sModelPath (opcional) - path del modelo en donde deben ser guardados los resultados de la consulta. Si no se especifica, los resultados serán retornados al finalizar el proceso 
		 * @param {string} sType - tipo de sistema de consulta: "scp" (SCP) o "erp"(ERP)
		 * @param {string} sGroupProperty (opcional) - luego de recibir la respuesta de la consulta, especificar si debe hacer un agrupamiento de los datos
		 * @return {object} respuesta del servicio
		 * @memberof mo-gestion-registros
		 */
		useReadOmodel: function (sTableName, sOdataParams, sModelPath, sType, sGroupProperty) {
			return new Promise((resolve, reject) => {
				var oModel = this.getView().getModel();
				var oContext = this;
				if (sOdataParams) {
					if (sOdataParams.substr(0, 1) === "(") {
						sOdataParams = sOdataParams ? sOdataParams : "";
					} else {
						sOdataParams = sOdataParams ? "?$filter=" + sOdataParams : "";
					}
				} else {
					sOdataParams = "";
				}
				this.oDataModel[sType].read("/" + sTableName + sOdataParams, {
					success: (oResponse) => {
						if (sModelPath) {
							if (oResponse.results.length > 0) {
								if (Array.isArray(sModelPath)) {
									sModelPath.forEach(sCurrentModelPath => {
										oModel.setProperty(sCurrentModelPath, oResponse.results);
									});
								} else {
									oModel.setProperty(sModelPath, oResponse.results);
								}
							} else {
								if (Array.isArray(sModelPath)) {
									sModelPath.forEach(sCurrentModelPath => {
										oModel.setProperty(sCurrentModelPath, null);
									});
								} else {
									oModel.setProperty(sModelPath, null);
								}
							}
						}
						if (oResponse.results) {
							resolve(oResponse.results);
						} else {
							resolve(oResponse);
						}
					},
					error: function (err) {
						let oError = {
							mensaje: "Error cargando tabla " + sTableName,
							data: err
						};
						if (sTableName === "/ZXC_PROJECTWITHVERSION_CDS/ZXC_PROJECTWITHVERSION") {
							oError.isCritical = true;
						}
						reject(oError);
					}
				});

			});
		},

	});
});